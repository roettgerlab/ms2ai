import os.path
import re
import shutil
from distutils.dir_util import copy_tree

from pymongo.errors import BulkWriteError

from storage.db_init import _get_dbs
from storage.serialization import _create_file
from utils.common import _validated_input


def _check_for_location(input_args, from_db, to_db, query, db):
    from_mongodb = _get_dbs(from_db, db).find_one(query)
    to_mongodb = _get_dbs(to_db, db).find_one(query)

    if not from_mongodb and to_mongodb and _validated_input(f'{from_db} does not contain peptide files, but {to_db} does! Swap the databases?',
                                                            valid_values=['y', 'n'], equals_to='y', default_option='y', force_default=input_args.force_default):
        return to_db, from_db

    elif not from_mongodb:
        quit(f'Peptide files not available in {from_db}')

    return from_db, to_db


def _query(input_args, from_db, db):
    peptides_query = {'_id': {'$nin': ['sizes', 'index']}}
    files_query = {}

    peptides = _get_dbs(from_db, db)
    files = _get_dbs('ms2ai', 'files')

    operator_dict = {'=': '$eq', '<': '$lt', '<=': '$lte', '>': '$gt', '>=': '$gte', 'in': '$in', 'nin': '$nin', 'regex': '$regex', 'exists': '$exists'}
    for query in input_args.query:
        # Finding get operator, key and value for each query
        key, value = re.split('|'.join(list([f' {f} ' for f in operator_dict.keys()])), query)
        operator = operator_dict[re.findall('|'.join(list([f' {f} ' for f in operator_dict.keys()])) + '|$', query)[0].strip()]
        collection = peptides if peptides.find_one({key: {'$exists': True}}) else files if files.find_one({key: {'$exists': True}}) else None
        if collection is None:
            print(f'{key} not found in any database, skipping!{"": <50}')
            continue
        value = value.split(' ') if operator in ['$in', '$nin'] else int(value) if operator == '$exists' else value
        value_type = list if operator in ['$in', '$nin'] else bool if operator == '$exists' else type(collection.find_one({key: {'$exists': True}})[key])
        specified_mongo_query = {key: {operator: re.escape(value_type(value)) if operator == '$regex' else value_type(value)}}
        eval(f'{"files" if collection.name == "files" else "peptides"}_query').update(specified_mongo_query)

    if files_query:
        peptides_query.update({'identifier': {'$in': files.find(files_query).distinct('identifier')}})
    return peptides_query


def _coarse_query(from_db, db_query, db, input_args):
    peptides = _get_dbs(from_db, db)
    coarse = True if (all([f in ['accession', 'raw file', 'identifier'] for f in list(db_query.keys())[1:]]) and not input_args.filtered) else False
    db_query = db_query if not coarse else {'_id': {'$ne': 'sizes'}, 'identifier': {'$in': peptides.find(db_query).distinct('identifier')}}

    return db_query, coarse


def _move_physical(input_args, from_db, to_db, query, coarse, db):
    print(f'File transfer initiated!{"": <50}', end='\r')
    data = _get_dbs(from_db, db).find(query).distinct('identifier') if coarse else [f['_id'] for f in _get_dbs(from_db, db).find(query)]
    move_files = data if coarse else [f'{f}.txt'.split('/', 1)[-1] for f in data]
    path = f'{input_args.path}images/'
    _create_file(f'{path}{to_db}')
    print(f'Executing file transfer{"": <50}', end='\r')
    if not coarse:
        [_create_file(f) for f in list(set([f'{path}{to_db}/{f.split("/", 1)[-1].rsplit("/", 1)[0]}' for f in data]))]

    move_cmd = (copy_tree if coarse else shutil.copy) if input_args.copy else shutil.move
    for i, f in enumerate(move_files):
        if i % (10 if coarse else 1000) == 0:
            print(f'{i}/{len(move_files)} files moved{"": <50}', end='\r')
        if os.path.exists(f'{path}{from_db}/{f}'):
            move_cmd(f'{path}{from_db}/{f}', f'{path}{to_db}/{f}')
    print(f'File transfer executed!{"": <50}', end='\n')


def _move_db_entries(input_args, from_db, to_db, query, db):
    print(f'Database transfer initiated!{"": <50}', end='\r')
    from_peptides = _get_dbs(from_db, db)
    to_peptides = _get_dbs(to_db, db)
    data = list(from_peptides.find(query)) if not input_args.keep_references else from_peptides.find(query)

    if not input_args.keep_references:
        [f.update({'_id': f['_id'].replace(from_db, to_db)}) for f in data]
    print(f'Executing database transfer{"": <50}', end='\r')
    if not _get_dbs(to_db, 'peptides').find_one({'_id': 'sizes'}) and _get_dbs(from_db, 'peptides').find_one({'_id': 'sizes'}):
        _get_dbs(to_db, 'peptides').insert_one(_get_dbs(from_db, 'peptides').find_one({'_id': 'sizes'}))
    if not _get_dbs(to_db, 'filtered').find_one({'_id': 'index'}) and _get_dbs(from_db, 'filtered').find_one({'_id': 'index'}):
        _get_dbs(to_db, 'filtered').insert_one(_get_dbs(from_db, 'filtered').find_one({'_id': 'index'}))

    try:
        to_peptides.insert_many(data, ordered=False, bypass_document_validation=True)
    except BulkWriteError as e:
        print(len(e.details['writeErrors']), 'duplicated database entries skipped')

    if input_args.move:
        from_peptides.delete_many(query)
    print(f'Database transfer executed!{"": <50}', end='\n')


def _move_data(input_args):
    move_or_copy = input_args.copy if input_args.copy else input_args.move
    from_db = move_or_copy[0]
    to_db = move_or_copy[1]
    db = 'filtered' if input_args.filtered else 'peptides'
    print(f'Conducting inital check and query setup{"": <50}', end='\r')
    query = _query(input_args, from_db, db)
    from_db, to_db = _check_for_location(input_args, from_db, to_db, query, db)
    query, coarse = _coarse_query(from_db, query, db, input_args)
    print(f'Preparing to move files{"": <50}', end='\r')
    if input_args.db_entries:
        _move_db_entries(input_args, from_db, to_db, query, db)
    if input_args.physical:
        _move_physical(input_args, from_db, to_db, query, coarse, db)
