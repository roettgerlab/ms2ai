import os
import shutil
from glob import glob

import pandas as pd
from pymongo import MongoClient
from pymongo.errors import BulkWriteError

from extractor.common import _import_table
from storage.utils import _get_path


def _add_sequence_to_metadata():
    from pymongo import MongoClient
    for db in MongoClient().list_databases():
        c_db = MongoClient()[db['name']]['peptides']
        count = c_db.count_documents({'_id': {'$regex': '\/\d*$'}})
        if count > 0:
            print(db['name'])
        for i, g in enumerate(c_db.find({'_id': {'$regex': '\/\d*$'}})):
            if i % 1000 == 0:
                print(f'{i}/{count}', end='\r')
            old = g['_id']
            new = f'{g["_id"]}-{g["sequence"]}'
            g.update({'_id': new})
            try:
                c_db.insert_one(g)
                if os.path.exists(f'{_get_path()}images/{old}'):
                    shutil.move(f'{_get_path()}images/{old}', f'{_get_path()}images/{new}')
                c_db.delete_one({'_id': old})
            except Exception as e:
                print(e, end='\r')
                pass


def _lowercase():
    a = [f.replace('\\', '/') for f in glob(f'{_get_path()}PXD*/**/evidence.txt')]
    for i, f in enumerate(a):
        print(f'{i}/{len(a)}{"": <50}', end='\r')
        try:
            b = _import_table(f)
            b.columns = [f.lower() for f in b.columns]
            pd.DataFrame.to_csv(b, f, index=False)
            b = b.to_dict(orient='records')
            try:
                MongoClient()['lowercase']['peptides'].insert_many(b)
            except BulkWriteError as e:
                print(len(e.details['writeErrors']), 'duplicated database entries skipped')
        except Exception as e:
            print(f, e)


def _run_corrector(cor):
    if cor == 'seq':
        _add_sequence_to_metadata()

    if cor == 'lowercase':
        _lowercase()