import json
from collections import defaultdict

import pandas as pd

from network.dlomix import _intensity_columns, _rt_columns
from storage.db_init import _get_dbs


def _remap_columns(key, target_key):
    column_map = {'raw file': 'raw_file', 'retention time': 'retention_time', 'accession': 'project',
                  'score': 'andromeda_score', 'modified sequence': 'modified_sequence', 'used scan': 'scan_number',
                  'charge': 'precursor_charge', 'intensity': 'precursor_intensity', 'fragmentation': 'fragmentation',
                  'mass analyser': 'mass_analyzer', 'length': 'peptide_length', 'nce': 'orig_collision_energy', 'm/z': 'mz [da]',
                  'ms/ms m/z': 'precursor_mz [da]', 'label': target_key}
    return column_map[key] if key in column_map else key


def _annotation_dict(input_args, df):
    annotations = defaultdict(dict)
    [annotations[f.split('/')[0]].update({f.split('/')[1]: f'{input_args.path}{f}/annotations.parquet'}) for f in df.identifier.unique()]
    annotations = dict(annotations)
    del df['label']
    del df['identifier']
    return annotations, df


def _create_parquet(input_args, target_key):
    peps = _get_dbs(input_args.db_name, 'filtered')
    df = pd.DataFrame(peps.find({'_id': {'$ne': 'index'}}, _intensity_columns() if target_key == 'annotation' else _rt_columns()))
    annotations, df = _annotation_dict(input_args, df) if target_key == 'annotation' else ({}, df)
    del df['_id']
    df.columns = [_remap_columns(f, target_key) for f in df.columns]
    return df, annotations


def _save_parquet(input_args, target_key):
    df, annotations = _create_parquet(input_args, target_key)
    df.to_parquet(f'{input_args.path}metadata/dlomix.parquet')
    return annotations


def _create_output(input_args, annotations, target_key):
    target_key = target_key if not target_key == 'annotation' else 'intensities'
    output = {'metadata': f'{input_args.path}metadata/dlomix.parquet',
             'annotations': annotations,
             'parameters': {
                 'target_column_key': target_key}
             }

    with open(f'{input_args.path}metadata/dlomix.txt', 'w') as file:
        file.write(json.dumps(output))


def _prepare_dlomix(input_args):
    target_key = _get_dbs(input_args.db_name, 'filtered').find_one({'_id': 'index'})['category']
    print(f'Saving the filtered data to parquet{"": <50}', end='\r')
    annotations = _save_parquet(input_args, target_key)
    print(f'Creating dlomix interaction files{"": <50}', end='\r')
    _create_output(input_args, annotations, target_key)
    print()




