import os
from sys import platform


def full_tutorial():
    ms2ai_path = __file__.replace('\\', '/').rsplit("/", 2)[0]
    print('TUTORIAL: Installing pre-required python packages')
    os.system(f'pip install --user -r "{ms2ai_path}/requirements.txt" {">nul" if platform.startswith("win") else ">/dev/null 2>&1"}')

    print('TUTORIAL: Checking pre-required python packages')
    os.system(f'python "{ms2ai_path}/utility_api.py" -p')

    print('TUTORIAL: Retrieving data from pride accession PXD019564')
    os.system(f'python "{ms2ai_path}/extractor_api.py" -e PXD019564')

    print('\nTUTORIAL: Filtering data based on score and PTMs (oxidation)')
    os.system(f'python "{ms2ai_path}/filter_api.py" -f Modifications -cl "Unmodified" "Oxidation (M)"')
    os.system(f'python "{ms2ai_path}/add_api.py" -a1 "m/z*" "retention time*" -a2 fragmentation NCE')

    print('\nTraining new model')
    os.system(f'python "{ms2ai_path}/network_api.py" -t ms2ai -tl 1 2 + ++ -e 5 -bs 16 -tp 80 -vp 20 -tpp 0')

    """ 
    Run in single -tag format (requires the model file to be in the right location): 
    python ms2ai.py -e PXD019564 -f Modifications -cl "Unmodified" "Oxidation (M)" -a1 "m/z*" "Retention time*" -a2 reset -n both -t 
    """
