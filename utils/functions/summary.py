import math
import os
from itertools import chain

import numpy as np
from pymongo import MongoClient

from extractor.common import _reset_data
from utils.common import _validated_input


def _convert_size(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])


def _print_option(first, last, counted_list):
    [print(f'({i + first}) {f[0]}: {f[1]} occurances') for i, f in enumerate(counted_list[first:last])]


def _print_inital_info(input_args):
    databases = []
    for db in MongoClient().list_databases():
        current_db = MongoClient()[db['name']]
        cols = {'peptides': 'peptides' in current_db.list_collection_names(),
                'filtered': 'filtered' in current_db.list_collection_names()}
        if any(list(cols.values())):
            storage_db = _convert_size(db['sizeOnDisk'])
            peptide = current_db.command("collstats", 'peptides')["count"] if cols['peptides'] else 0
            filtered = current_db.command("collstats", 'filtered')["count"] if cols['filtered'] else 0
            databases.append(current_db.name)
            average_storage = np.mean([os.path.getsize(f'{input_args.path}images/{f["_id"]}.txt') for f in current_db['peptides'].find()[1:100]]) * peptide \
                              if os.path.exists(f'{input_args.path}images/{current_db.name}') else 0
            print(f'DB: {current_db.name}. Storage (DB): {storage_db}. Total peptides: {peptide}, Estimated Storage: '
                f'{_convert_size(0) if np.isnan(average_storage) else _convert_size(average_storage)}. Total filtered peptides {filtered}')

    current_db = MongoClient()['ms2ai']
    storage = round(current_db.command("collstats", "projects")['size'] * 1e-9, 4)
    metadata = current_db.command("collstats", "projects")['count']
    databases.append('projects')
    print(f'DB: projects. Storage: {storage} gb. Total project entries: {metadata}. Extend for more information on metadata')

    return _validated_input('\nChoose DB to extend: ', databases, show_options=False)


def _specific_info(input_args, collection, query, database_name, query_name=None):
    def _return(database_name):
        if database_name == 'projects':
            return _extended_pride_info(input_args)
        else:
            return _extended_peptide_info(input_args, database_name)

    if query_name is None:
        query_name = query

    print(f'Calculating!{"": <50}', end='\r')
    count_aggregation = list(collection.aggregate([
        {'$group': {'_id': f'${query}', 'count': {'$sum': 1}}}]))
    counted_list = sorted([list(f.values()) for f in count_aggregation], key=lambda x: x[1], reverse=True)
    print(f'Done!{"": <50}', end='\r')

    start_value = 0
    end_value = 10
    _print_option(start_value, end_value, counted_list)
    possible_options = list(chain.from_iterable([['', 'next'], [str(f) for f in range(0, end_value)]]))
    next_or_back = _validated_input(f'\nOptions: Value for projects with given {query_name}. '
                                     f'Press enter to go back. Type "next" to get next elements in the list! ',
                                    possible_options, show_options=False)
    while next_or_back == 'next':
        start_value = end_value
        end_value = end_value + 10
        _print_option(start_value, end_value, counted_list)
        possible_options = list(chain.from_iterable([['', 'next'], [str(f) for f in range(0, end_value)]]))
        next_or_back = _validated_input(f'\nOptions: Value for projects with given {query_name}. '
                                         f'Press enter to go back. Type "next" to get next elements in the list! ',
                                        possible_options, show_options=False)

    if next_or_back == '':
        return _return(database_name)

    else:
        relevant_projects = list(reversed(collection.find({**{query: counted_list[int(next_or_back)][0]}, **({'maxquant': True} if collection.name == 'projects' else {})}).distinct('accession')))
        print(f"Projects: {' '.join(relevant_projects)}")
        input('Hit enter to go back!')
        return _return(database_name)


def _extended_pride_info(input_args):
    data = MongoClient()['ms2ai']['projects']
    print(f'\nPRIDE choosen! See list for additional info{"": <50}', end='\r')
    info_options = ['Organisms', 'Organism parts', 'Diseases', 'MS Instruments', 'Softwares', 'Quantification Methods', 'PTMs', 'Go back!']
    actual_name_list = ['organisms', 'organismParts', 'diseases', 'instruments', 'softwares', 'quantificationMethods', 'identifiedPTMStrings']

    [print(f'({i}) {f}') for i, f in enumerate(info_options)]
    requested_info = _validated_input(f'Request information on: ', [str(f) for f in range(len(info_options))], show_options=False)

    if int(requested_info) + 1 == len(info_options):
        print(f'\nReturning to DB selection!{"": <50}', end='\r')
        _run_summary(input_args)

    _specific_info(input_args, data, actual_name_list[int(requested_info)], 'projects', query_name=info_options[int(requested_info)])
    print()


def _resolution_info(input_args, peptide_data, database_name):
    print(f'\nMS1 Resolution: {peptide_data.find_one()["ms1size"]} \nMS2 Resolution: {peptide_data.find_one()["ms2size"]}')
    input('Press enter to go back!')
    return _extended_peptide_info(input_args, database_name)


def _extended_peptide_info(input_args, database_name):
    db = MongoClient()[database_name]
    peptides = db['peptides']
    print(f'\n{database_name} chosen! See list for additional information')
    info_options = ['Image resolutions', 'Charge states', 'Sequences', 'Sequence lengths', 'Proteins', 'Modifications (PTMs)', 'Delete data!', 'Go back!']
    actual_name_list = ['', 'charge', 'sequence', 'length', 'proteins', 'modifications']
    [print(f'({i}) {f}') for i, f in enumerate(info_options)]
    requested_info = _validated_input('Request information on:', [str(f) for f in range(len(info_options))], show_options=False)
    if int(requested_info) == len(info_options) - 2:
        input_args.db_name = database_name
        _reset_data(input_args)
        _run_summary(input_args)
    if int(requested_info) == len(info_options) - 1:
        print(f'\nReturning to DB selection!{"": <50}', end='\r')
        _run_summary(input_args)
    elif requested_info == '0':
        _resolution_info(input_args, peptides, database_name)
    else:
        _specific_info(input_args, peptides, actual_name_list[int(requested_info)], database_name, query_name=info_options[int(requested_info)])


def _run_summary(input_args):
    requested_db = _print_inital_info(input_args)

    if requested_db == 'projects':
        _extended_pride_info(input_args)
    else:
        _extended_peptide_info(input_args, requested_db)
