from itertools import chain
from json import loads

import pandas as pd
import xmltodict
import yaml

from extractor.common import _import_table
from extractor.init_local import _new_accnr
from extractor.xml_parser import _flatten_dict, _xml_keys
from storage.db_init import _get_dbs, _insert_or_update


def _yaml_parser(path):
    yaml_file = yaml.safe_load(open(path).read())
    data = list(chain(*[yaml_file[f] for f in yaml_file]))
    yaml_data = {}
    [yaml_data.update({f['name']: [f['value']]}) for f in data]

    return pd.DataFrame.from_dict(yaml_data)


def _get_project(input_args, data):
    projectname_column = [f for f in data.columns if f.lower() in ['accession', 'accession2', 'project', 'projects']]
    if 'accession' in data.columns:
        data = data.rename(columns={projectname_column[0]: 'accession'})
    else:
        projectname_column = _new_accnr(input_args, default_option='use')
        data['accession'] = projectname_column
    return data


def _get_files(data):
    filename_column = [f for f in data.columns if f.lower() in ['raw file', 'filename', 'file name']]
    if not filename_column:
        quit('No column to cross-reference with file names.')
    else:
        data = data.rename(columns={filename_column[0]: 'raw file'})
        # data['Raw file'] = data['Raw file'].apply(str)
        data['raw file'] = data['raw file'].str.rsplit('.', 1, expand=True)[0]
    return data


def _import_data(path):
    if path.endswith('json'):
        data = loads(open(path).read())
        data = pd.DataFrame.from_dict(data)

    elif path.endswith('yaml'):
        data = _yaml_parser(path)

    elif path.endswith('xml'):
        xml_dict = dict(xmltodict.parse('\n'.join(open(path).readlines()[1:])))
        if len(xml_dict) == 1:
            xml_dict = xml_dict[list(xml_dict.keys())[0]]
        xml_dict = _flatten_dict(xml_dict)
        data = [{'raw file': g.replace('\\', '/').split('/')[-1].split('.')[0]} for g in xml_dict['filePaths']]
        base_info = {f: xml_dict[f] for f in xml_dict if f in _xml_keys()}
        [f.update(base_info) for f in data]
        data = pd.DataFrame.from_dict(data)

    else:
        data = _import_table(path)
        if path.endswith('parameters.txt'):
            data = data.T
            data.columns = data.iloc[0]
            data = data[1:]

    return data


def _add_metadata(input_args, path, file):
    print(f'Appending data from {path} to the projects database')
    data = _import_data(path.lower())
    data = _get_project(input_args, data)

    if file == 'files':
        data = _get_files(data)
        data = pd.DataFrame.to_dict(data, 'records')
        files = _get_dbs('ms2ai', 'files')
        [f.update({'identifier': f'{f["accession"]}/{f["raw file"]}'}) for f in data]
        [_insert_or_update(files, f, ['accession']) for f in data]

    if file == 'projects':
        data = pd.DataFrame.to_dict(data, 'records')
        projects = _get_dbs('ms2ai', 'projects')
        _insert_or_update(projects, data[0], ['accession'])
