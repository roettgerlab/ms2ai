def __package_test():
    try:

        from storage.db_init import _get_current_pride
        from utils.functions.add_metadata import _add_metadata
        from scraper.run import _run_scraper
        from filter.run import _run_filter
        from network.wandb.sweep import _wandb_sweep
        from network.run import _run_network
        from utils.functions.move import _move_data
        from utils.functions.tutorial import full_tutorial
        from utils.functions.summary import _run_summary
        from utils.functions.move import _move_data
        from utils.functions.dlomix import _prepare_dlomix
        from utils.functions.corrector import _run_corrector
        from utils.functions.add_metadata import _add_metadata
        from extractor.run import _run_extractor
        try:
            import fundamentals
        except:
            print('fundamentals not installed. Without this you wont be able to annotate spectra for intensity analysis\n to install run:\n'
                  'pip install git+https://github.com/wilhelm-lab/spectrum_fundamentals@abfe6bc21e0e272103f83d54417082260860e8f5')
        try:
            import dlomix
        except:
            print('DLOmix not installed. Without this you wont be able to run dlomix models\n to install run:\n'
                  'pip install dlomix')
        try:
            import wandb
        except:
            print('DLOmix not installed. Without this you wont be able to run dlomix models\nto install run:\n'
                  'pip install wandb')
        print('Success, all necessary packages are installed')
    except Exception as e:
        package = str(e).split("'")[1]
        quit(f'Missing package: {package}')
