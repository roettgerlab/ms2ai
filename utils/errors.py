class AttemptsThrottling(Exception):
    "Run out of attempts. Likely cause rate throttling"
    pass


class DownloadError(Exception):
    "Unknown Download error"
    pass


class BrokenMQFile(Exception):
    "Raised when the MaxQuant file is broken"
    pass


class RawfileMismatch(Exception):
    "Rawfiles and MQ files did not match"
    pass


class NotMaxquantZip(Exception):
    "Zipfile does not contain MaxQuant information"
    pass
