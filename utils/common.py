import os
import re
from io import BytesIO
from os.path import splitext
from random import sample
from time import sleep
from timeit import default_timer as timer
from urllib.error import URLError
from urllib.request import urlopen
from zipfile import ZipFile

from storage.serialization import _create_file
from storage.utils import _get_path
from utils.errors import AttemptsThrottling, DownloadError


def _inital_path_setup():
    current_path = __file__.replace('\\', '/').rsplit("/", 2)[0]
    if not os.path.exists(f'{current_path}/path.txt'):
        path = input('No data path has been setup. Please input location for data to be stored: ')
        _create_file(path)
        with open(f'{current_path}/path.txt', 'w') as file:
            file.write(path)
        print(f'Data path stored in {current_path}/path.txt. In order to change data path, please delete this file.')


def _compile_args(parser):
    try:
        input_args = parser.parse_args()
        input_args.path = _get_path()
        return input_args
    except:
        parser.print_help()
        quit()


def _validated_input(prompt, valid_values, equals_to=None, default_option=None, show_options=True, force_default=False):
    def __to_default(input_value):
        if default_option is not None and input_value == '':
            return default_option
        else:
            return input_value

    if force_default:
        return equals_to == default_option

    value = input(f'{prompt} {" / ".join(valid_values if default_option is None else [f if f != default_option else f"[{f.upper()}]" for f in valid_values])}: ') if show_options else input(prompt)
    value = __to_default(value)
    while value not in valid_values:
        value = input(f'{prompt} {" / ".join(valid_values if default_option is None else [f if f != default_option else f"[{f.upper()}]" for f in valid_values])}: ') if show_options else input(prompt)
        value = __to_default(value)

    if equals_to is not None:
        return value == equals_to
    else:
        return value


def _all_string_match(string, pattern, overlap=False):
    if overlap:
        return [m.start() for m in re.finditer('(?=%s)(?!.{1,%d}%s)' % (pattern, len(pattern) - 1, pattern), string)]
    else:
        return [m.start() for m in re.finditer(pattern, string)]


def _match_extensions(path, multi_exts=False):
    if bool(multi_exts):
        for ext in multi_exts:
            if path.endswith(ext):
                return path[-len(ext):]
    return splitext(path)[1]


def _url_open_zip(url, multiprocessing, attempts=10):
    try:
        sleep(sample(range(1, multiprocessing*2), 1)[0]) if multiprocessing else ''
        data = urlopen(url).read()
        return ZipFile(BytesIO(data))
    except URLError:
        raise AttemptsThrottling if attempts == 0 else _url_open_zip(url, multiprocessing, attempts=attempts - 1)
    except Exception as e:
        raise DownloadError


def _timer(func):
    def wrapper():
        start = timer()
        func()
        print(f'Time elaborated: {timer() - start} seconds')

    return wrapper


def _get_sizes(path, filelist):
    import os
    from glob import glob
    a = {}
    for f in filelist:
        a[f] = sum([os.path.getsize(f) for f in glob(f'{path}{f}')]) / 1024 / 1024 / 1024
    print(a, sum(a.values()))
