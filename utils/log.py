import logging
import os

from storage.serialization import _create_file


def _ms2ai_log(msg, level):
    logging.log(logging._checkLevel(level.upper()), msg)


def _setup_log(input_args):
    if not os.path.exists(f'{input_args.path}metadata/'):
        _create_file(f'{input_args.path}metadata/')
    return logging.basicConfig(filename=f'{input_args.path}metadata/log.txt',
                               filemode='a',
                               format='%(asctime)s (%(levelname)s) | %(message)s',
                               datefmt='%d-%m-%Y %H:%M:%S',
                               level=input_args.logging_level.upper())
