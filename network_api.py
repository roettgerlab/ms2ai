import argparse

from network.run import _run_network
from utils.common import _compile_args, _inital_path_setup
from utils.log import _setup_log


def _setup_args():
    parser = argparse.ArgumentParser(description='MS2AI Peptide metadata filtering')
    # Setup
    parser.add_argument('-t', '--train_model', type=str, help='Trains a model based on the model specified', metavar="")
    parser.add_argument('-tm', '--test_model', type=str, help='Run neural network tests on a specific model in the metadata folder', metavar="")
    parser.add_argument('-1', '--ms1_input', help='Add MS1 information to model architecture', action='store_true')
    parser.add_argument('-2', '--ms2_input', help='Add MS2 information to model architecture', action='store_true')
    parser.add_argument('-1a', '--ms1_add_input', type=str, nargs='+', help='Add additional information to the MS1 input', default=[])
    parser.add_argument('-2a', '--ms2_add_input', type=str, nargs='+', help='Add additional information to the MS2 input', default=[])
    parser.add_argument('-s', '--seed', type=int, help='Set a numeric replication seed for data splits', metavar="", default=False)
    parser.add_argument('-p', '--print_model', help='Print summary output of model', action='store_true')
    # Hyper parameters
    parser.add_argument('-c', '--channels', type=str, nargs='+', help='Channels used when training the ms2ai network', metavar="", default=["mean", "var", "max", "count", "mz"])
    parser.add_argument('-seq', '--sequence_length', type=int, help='Configure the sequence length for DLoMIX retention time predictors', metavar="", default=30)
    parser.add_argument('-spe', '--spectra_length', type=int, help='Configure the length of non binned ms2 input', metavar="", default=100)
    parser.add_argument('-e', '--epochs', type=int, help='Configure the amount of epochs to run the network', metavar="", default=50)
    parser.add_argument('-bs', '--batch_size', type=int, help='Configure the batch size for the network', metavar="", default=256)
    parser.add_argument('-lr', '--learning_rate', type=float, help='Configure the learning rate for the network', metavar="", default=0.005)
    parser.add_argument('-es', '--early_stopping', type=int, help='Configure the early stopping overfitting parameter for the network', metavar="", default=10)
    parser.add_argument('-tv', '--testing_validation', help='Validation on the testing set every epoch along with the validation data', action='store_true')
    # Data splitting
    parser.add_argument('-sos', '--split_on_sequence', help='Performs training/validation split on unique sequences from the same files instead of random split', action='store_true')
    parser.add_argument('-is', '--identifier_split', help='Split on Raw file identifier instead of project accession', action='store_true')
    parser.add_argument('-vs', '--validation_split', type=float, help='Configure the validation data split', metavar="", default=20)
    parser.add_argument('-ts', '--test_split', type=float, help='Configure the testing data split', metavar="", default=10)
    parser.add_argument('-a', '--avoid', help='Avoids testing on data from projects or rawfiles (-is) used for training', action='store_false')
    # Tracking
    parser.add_argument('-tb', '--tensorboard', help='Enable tensorboard model tracking', action='store_true')
    parser.add_argument('-w', '--wandb', help='Enable WandB model tracking', action='store_true')
    parser.add_argument('-pn', '--wandb_project_name', type=str, help='Project name for the WandB tracking', metavar='', default='ms2ai')
    parser.add_argument('-tn', '--wandb_team_name', type=str, help='Entity name for the WandB tracking', metavar='', default='')
    # WandB Sweep
    parser.add_argument('--sweep', help='Run a WandB sweep of hyperparameters', action='store_true')
    parser.add_argument('--sweep_id', type=str, help='Sweep name, to connect with existing WandB sweep agent', metavar="")
    parser.add_argument('--fetch_id', type=str, help='Fetch the model(s) of a performed sweep', metavar="")
    parser.add_argument('--delete_id', type=str, help='Delete .h5 files to retain space from sweep (keep n best files)', metavar="")
    # Default setup parameters
    parser.add_argument('-db', '--db_name', type=str, help='Add information to the MS1 part of the peptide data representation', metavar="", default='ms2ai')
    parser.add_argument('-l', '--logging_level', type=str, help='Python logging level for internal error prints (https://docs.python.org/3/library/logging.html#logging-levels)', metavar="", default='info')
    parser.add_argument('-f', '--force_default', help='Always choose the default for all user inputs (if one exists)', action='store_true')
    parser.add_argument('-wtf', '--write_to_file', help='Write output metrics to a csv file format', action='store_true')
    parser.add_argument('-id', '--inference_dropouts', type=int, help='Amount of tests per datapoint for inference dropout testing', default=0)
    parser.add_argument('-ids', '--inference_dropout_size', type=int, help='Amount of tests per datapoint for inference dropout testing', default=100000)
    parser.add_argument('-ph', '--plot_history', help='Plots the validation history of training', action='store_false')
    parser.add_argument('-v', '--verbose', help='Show outputs in the terminal', action='store_false')
    parser.add_argument('-wo', '--workers', type=int, help='Multiprocessing workers used for TF training', default=1)

    return parser


if __name__ == '__main__':
    _inital_path_setup()
    input_args = _compile_args(_setup_args())
    _setup_log(input_args)

    wandb_params = {'apply': input_args.wandb, 'project name': input_args.wandb_project_name, 'team name': input_args.wandb_team_name}
    if input_args.train_model or input_args.test_model:
        _run_network(input_args, wandb_params)
    elif input_args.sweep or input_args.sweep_id or input_args.fetch_id or input_args.delete_id:
        from network.wandb.sweep import _wandb_sweep
        _wandb_sweep(input_args)
