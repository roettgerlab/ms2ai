# MS2AI: Mass Spectrometry Pipeline
###### Research paper release version (1.0) available [here](https://gitlab.com/tjobbertjob/ms2ai-paper-release). changelog.txt contains all changes
###### The current version removed ThermoRawFileParser and instead imports raw files directly into python using fisher_py. If you wish to use TRFP and get the mzML files, use the legacy version [here](https://gitlab.com/TobiasRehfeldt/ms-2-ai-legacy-trfp)
## Requirements
OS: Linux, _macOS_ or Windows with Python 3.8

MongoDB: A working version of MongoDB with the server running

## Installation
### Linux & MacOS:
```
sudo apt-get update -y
sudo apt-get upgrade -y
wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.11.0-Linux-x86_64.sh
sh Miniconda3-py38_4.11.0-Linux-x86_64.sh
rm Miniconda3-py38_4.11.0-Linux-x86_64.sh
source ~/.bashrc
conda install -c conda-forge pythonnet==2.5.2 -y

git clone https://gitlab.com/roettgerlab/ms2ai
cd ms2ai
pip install -r requirements.txt
pip install tensorflow

# To use DLOmix models:
pip install dlomix
# To use Weights and Biases
pip install wandb
```

#### Linux MongoDB:
```
sudo apt-get install mongodb -y
sudo service mongodb start
```
#### MacOS MongoDB:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)
brew install node
brew install mongo
sudo mkdir -p /data/db
sudo chmod -arwx /data/db
```

### Windows:
```
pip install pythonnet==2.5.2

git clone https://gitlab.com/roettgerlab/ms2ai
cd ms2ai
pip install -r requirements.txt
pip install tensorflow

# To use DLOmix models:
pip install dlomix
# To use Weights and Biases
pip install wandb
```
* Download [MongoDB server](https://www.mongodb.com/try/download)
* Follow [the tutorial](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/).
* Download and add the [Database tools](https://www.mongodb.com/try/download/tools) to
  the [path variables](https://docs.microsoft.com/en-us/previous-versions/office/developer/sharepoint-2010/ee537574(v=office.14))

* Optional installation of [TensorFlow GPU](https://www.tensorflow.org/install/pip)