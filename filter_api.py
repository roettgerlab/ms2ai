import argparse

from filter.run import _run_filter
from utils.common import _compile_args, _inital_path_setup
from utils.log import _setup_log


def _setup_args():
    parser = argparse.ArgumentParser(description='MS2AI Peptide metadata filtering')
    # Class
    parser.add_argument('-c', '--category', type=str, help='Class on which the filtering category should be based on', metavar='')
    parser.add_argument('-b', '--balance_classes', help='Balances all classes for all rawfiles respectively', action='store_false')
    parser.add_argument('-cr', '--class_representation', help='Ignores all raw files where all classes arent present', action='store_false')
    parser.add_argument('-bc', '--binary_class', type=str, help='Added to the filter to create a binary filtering', metavar="")
    parser.add_argument('-cc', '--class_count', type=int, help='Added to the filter to create n different filter classes based on abundance', metavar="")
    parser.add_argument('-cl', '--class_list', type=str, nargs='+', help='Added to the filter to create n classes based on a list passed by the user', metavar="")
    parser.add_argument('-ol', '--overwrite_label', type=int, help='Creates a custom label for every peptide (useful for query specific labelling)', metavar="", default=False)
    # Query
    parser.add_argument('-q', '--query', type=str, nargs='+', help='A query added to the filter, to give the user more control over the filtered peptides', metavar="")
    parser.add_argument('-oq', '--or_query', type=str, nargs='+', help='A query which add an "or" between the statements', metavar="")
    parser.add_argument('-aq', '--and_query', type=str, nargs='+', help='A query which add an "and" between the statements', metavar="")
    parser.add_argument('-er', '--escape_regex', help='Escape special characters in regex', action='store_true')
    parser.add_argument('-sq', '--shape_query', help='Automatically query the ms1 shape to make sure there is no wrongly shaped peptides', action='store_true')
    parser.add_argument('-lo', '--limit_output', type=int, help='Limit the output to n values of each class', metavar="", default=False)
    parser.add_argument('-li', '--linearize', type=int, help='Linearize the output value between 0 and 1', metavar="", default=False)
    parser.add_argument('-n', '--normalize', type=str, nargs='+', help='Normalizes the output to be between 0 and 1', metavar="")
    # Setup
    parser.add_argument('-db', '--db_name', type=str, help='Add information to the MS1 part of the peptide data representation', metavar="", default='ms2ai')
    parser.add_argument('-l', '--logging_level', type=str, help='Python logging level for internal error prints (https://docs.python.org/3/library/logging.html#logging-levels)', metavar="", default='info')
    parser.add_argument('-f', '--force_default', help='Always choose the default for all user inputs (if one exists)', action='store_true')
    parser.add_argument('-nr', '--no_reset', help='If active, the previous version of the filtered data will not be reset, and the current filter will be added on top', action='store_true')

    return parser


if __name__ == '__main__':
    _inital_path_setup()
    input_args = _compile_args(_setup_args())
    _setup_log(input_args)

    _run_filter(input_args)
