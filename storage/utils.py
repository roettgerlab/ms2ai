from json import loads


def _config(config, *keys):
    cfg_dict = {'e': 'extractor', 's': 'storage', 'f': 'filter', 'n': 'network'}
    file_location = __file__.replace("\\", "/")
    with open(f'{file_location.rsplit("/", 2)[0]}/config.json') as file:
        cfg = loads(file.read())[cfg_dict[config]]
        values = [cfg[f] for f in keys]

    return values[0] if len(values) == 1 else values


def _get_path():
    current_path = __file__.replace('\\', '/').rsplit("/", 2)[0]
    with open(f'{current_path}/path.txt') as file:
        base = file.read()
    return base if base.endswith('/') else f'{base}/'
