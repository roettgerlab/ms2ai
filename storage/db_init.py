import os
from sys import platform

import requests
from pymongo import MongoClient

from utils.common import _validated_input


def _get_dbs(db_name, *args):
    dbs = [MongoClient()[db_name][str(f)] for f in args]
    return dbs[0] if len(dbs) == 1 else dbs


def _get_current_pride(input_args):
    if _get_dbs('ms2ai', 'projects').find_one() and _validated_input('Overwrite current project metadata originating from PRIDE?', ['y', 'n'], equals_to='y', default_option='n', force_default=input_args.force_default):
        _get_dbs('ms2ai', 'projects').delete_many({'source': 'pride'})

    db = requests.get('https://gitlab.com/tjobbertjob/ms2ai-database/-/raw/main/projects.bson.gz?inline=false')
    open(f'{input_args.path}/projects.bson.gz', 'wb').write(db.content)
    os.system(f'mongorestore{"" if platform.startswith("linux") or platform.startswith("darwin") else ".exe"} --gzip --archive="{input_args.path}/projects.bson.gz" --nsInclude="ms2ai.projects"')
    os.remove(f'{input_args.path}/projects.bson.gz')


def _insert_or_update(db, insert, *query):
    if isinstance(query[0], dict):
        query = query[0]
    else:
        query = dict([(f, insert[f]) for f in query[0]])
    if bool(db.find_one(query)):
        db.update_one(query, {'$set': insert})
    else:
        db.insert_one(insert)
