from io import BytesIO
from zipfile import *

from utils.common import _url_open_zip
from utils.log import _ms2ai_log


def _loose_to_zip(urls, multiprocessing):
    project = [f for f in urls[0].split('/') if 'PXD' in f][0]
    try:
        data = [_url_open_zip(f, multiprocessing) for f in urls]
        zip_elem = ZipFile(file=BytesIO(), mode='w')
        for name, elem in zip(urls, data):
            zip_elem.writestr(name, elem)

        return zip_elem

    except Exception as e:
        _ms2ai_log(f'{project}: {e}', 'info')


def _ext_from_zip(file_list, multiprocessing, zip_file):
    if not isinstance(zip_file, ZipFile):
        zip_file = _url_open_zip(zip_file, multiprocessing)
    return [[zip_file.open(file) for file in zip_file.filelist if name in file.orig_filename and 'MACOSX' not in file.orig_filename] for name in file_list]


def _managed_exts():
    return ['.zip']  # , '.tar.gz' , '.7z', '.rar', '.gz', '.gzip'
