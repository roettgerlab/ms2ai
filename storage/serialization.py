import os

import numpy as np


def _create_file(path):
    if not os.path.exists(path):
        os.makedirs(path)


def _np_save(obj, path):
    with open(path, 'wb') as f:
        np.save(f, obj, allow_pickle=True)


def _np_load(path):
    with open(path, 'rb') as f:
        return np.load(f, allow_pickle=True)
