import argparse

from scraper.run import _run_scraper
from storage.db_init import _get_current_pride
from utils.common import _compile_args, _inital_path_setup
from utils.log import _setup_log


def _setup_args():
    parser = argparse.ArgumentParser(description='MS2AI Peptide metadata filtering')
    # Inputs
    parser.add_argument('-a', '--accession', type=str, nargs='+', help='Extract metadata from a limited selection of accessions', metavar='')
    parser.add_argument('-db', '--fetch_db', help='Fetch the most up-to-date version of the projects database', action='store_true')
    # Setup
    parser.add_argument('-t', '--threads', type=int, help='Runs the scraper at n threads', metavar='', default=1)
    parser.add_argument('-z', '--max_zipfile_size', type=int, help='Max zip file size (in GB) set to reduce the memory stress', metavar='', default=4)
    parser.add_argument('-l', '--logging_level', type=str, help='Python logging level for internal error prints (https://docs.python.org/3/library/logging.html#logging-levels)', metavar="", default='info')
    parser.add_argument('-f', '--force_default', help='Always choose the default for all user inputs (if one exists)', action='store_true')

    return parser


if __name__ == '__main__':
    _inital_path_setup()
    input_args = _compile_args(_setup_args())

    _setup_log(input_args)

    if not input_args.fetch_db:
        _run_scraper(input_args)
    else:
        _get_current_pride(input_args)
