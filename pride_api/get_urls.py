from pride_api.common import _get_json, _get_link, _get_size
from pride_api.keys import _maxquant_file_names
from storage.compressions import _managed_exts, _loose_to_zip
from utils.common import _match_extensions


def _get_project(accession):
    return _get_json(f'https://www.ebi.ac.uk/pride/ws/archive/v2/projects/{accession}')


def _get_files_api(accession):
    return _get_json('https://www.ebi.ac.uk/pride/ws/archive/v2/files/byProject?accession=PXDxxxxxx'.replace('PXDxxxxxx', accession))


def _zip_filter(input_args, files_api):
    return _match_extensions(files_api['fileName'], ['.tar.gz']).lower() in _managed_exts() \
           and files_api['fileCategory']['value'] == 'SEARCH' \
           and files_api['fileSizeBytes'] * 1e-9 <= input_args.max_zipfile_size


def _get_zip_files(input_args, api, get_size=False):
    if get_size:
        return [[_get_link(f), _get_size(f)] for f in api if _zip_filter(input_args, f)]
    else:
        return [_get_link(f) for f in api if _zip_filter(input_args, f)]


def _raw_filter(files_api):
    return _match_extensions(files_api['fileName']).lower() == '.raw' and files_api['fileCategory']['value'] == 'RAW'


def _get_raw_files(api, get_size=False):
    if get_size:
        return [[_get_link(f), _get_size(f)] for f in api if _raw_filter(f)]
    else:
        return [_get_link(f) for f in api if _raw_filter(f)]


def _loose_MQs(api, input_args, multiprocessing, to_zip=False):
    mq_urls = [[_get_link(f), _get_size(f)] for f in api for g in _maxquant_file_names() if f['fileName'] == g]
    if sum([f[1] for f in mq_urls]) * 1e-9 >= input_args.max_zipfile_size:
        mq_urls = [f for f in mq_urls if f[0].split('/')[-1] in ['summary.txt', input_args.maxquant_file]]

    while mq_urls and sum([f[1] for f in mq_urls]) * 1e-9 >= input_args.max_zipfile_size:
        mq_urls.pop()

    loose_urls = [f[0] for f in mq_urls]
    if not to_zip or not loose_urls:
        return loose_urls
    else:
        print(f'Fetching loose metadata files', end='\r') if not multiprocessing else ''
        return _loose_to_zip(loose_urls, multiprocessing)


def _get_extensions(api):
    filetypes = list(set([f.lower() for f in [_match_extensions(f['fileName'], ['.tar.gz']) for f in api]]))

    search_filetypes = list(set([f.lower() for f in [_match_extensions(f['fileName'], ['.tar.gz']) for f in api
                                                     if f['fileCategory']['value'] == 'SEARCH']]))

    return {'filetypes': filetypes, 'search_filetypes': search_filetypes}


def _base_metadata_page(page_nr):
    return _get_json(f'https://www.ebi.ac.uk/pride/ws/archive/v2/projects?page={page_nr}&sortDirection=DESC&sortConditions=submissionDate')['_embedded']['projects']
