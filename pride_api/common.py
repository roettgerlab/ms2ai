import os
from time import sleep

import requests

from storage.db_init import _get_dbs
from storage.serialization import _create_file


def _file_downloader(location, accnr, link, multiprocessing, name=None):
    _create_file(location)
    if not os.path.exists(f'{location}/{link.split("/")[-1]}'):
        print(f'{accnr}/{link.split("/")[-1]}: Downloading file{"": <50}', end='\r') if not multiprocessing else ''
        # if input_args.threads > 8:
        #     sleep(sample(range(1, input_args.threads*2), 1)[0])
        s = requests.Session()
        resp = s.get(f'https{link[3:]}', stream=True, timeout=60)
        with open(f'{location}/{link.split("/")[-1] if name == None else name}', 'wb') as f:
            f.write(resp.content)


def _get_json(url):
    for attempts in range(50):
        try:
            return requests.get(url).json()
        except requests.exceptions.JSONDecodeError:
            return False
        except Exception as e:
            if requests.get(url).status_code == 204:  # This error code is only presented if the site doesnt exist
                return {}
            sleep(2)
    return {}


def _get_link(file):
    index = 0 if file['publicFileLocations'][0]['name'] == 'FTP Protocol' else 1
    return file['publicFileLocations'][index]['value']


def _get_size(file):
    return file['fileSizeBytes']


def _pride_projects(input_args):
    from extractor.run import _extractor_query
    input_query = _extractor_query(input_args)
    pride_query = {'maxquant_files': input_args.maxquant_file, 'source': 'pride', 'filetypes': {'$all': ['.zip', '.raw']}}
    accessions = _get_dbs('ms2ai', 'projects').find({**input_query, **pride_query}).distinct('accession')

    return accessions
