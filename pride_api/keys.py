def _pride_keys():
    return ['instruments', 'softwares', 'quantificationMethods', 'sampleAttributes',
            'organisms', 'organismParts', 'diseases', 'identifiedPTMStrings', 'source']


def _maxquant_file_names():
    return ['aifMsms.txt', 'allPeptides.txt', 'evidence.txt', 'libraryMatch.txt', 'matchedFeatures.txt',
            'modificationSpecificPeptides.txt', 'ms3Scans.txt', 'msms.txt', 'msmsScans.txt', 'msScans.txt',
            'mzRange.txt', 'Oxidation (M)Sites.txt', 'parameters.txt', 'peptides.txt', 'proteinGroups.txt', 'summary.txt']
