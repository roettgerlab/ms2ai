import os
from functools import partial
from time import sleep

import numpy as np

from network.init import _set_init, _get_params, _ms2ai_columns
from network.partitions import _get_partitions, _add_data
from storage.db_init import _get_dbs
from utils.common import _validated_input


def _sweep_config():
    sweep_config = {
        'method': 'random',
        'metric': {
            'name': 'val_accuracy',
            'goal': 'maximize'
        },
        'parameters': {
            # 'mean_input': {
            #     'values': [1, 0]
            # },
            # 'var_input': {
            #     'values': [1, 0]
            # },
            # 'max_input': {
            #     'values': [1, 0]
            # },
            # 'count_input': {
            #     'values': [1, 0]
            # },
            # 'mz_input': {
            #     'values': [1, 0]
            # },
            'input_dropout': {
                'values': [0, 0.1, 0.2, 0.3]
            },
            'kernel_size': {
                'values': [5, 7, 9, 11]
            },
            'pool_size': {
                'values': [2, 3, 5]
            },
            'kernel_count': {
                'values': [4, 6, 8, 10]
            },
            # 'dense_layers': {
            #     'distribution': 'uniform',
            #     'min': 2,
            #     'max': 10
            # },
            # 'dense_neurons': {
            #     'values': [32, 64, 128, 256]
            # },
            'learning_rate': {
                'distribution': 'uniform',
                'min': 0.0001,
                'max': 0.001
            },
            'decay_rate': {
                'distribution': 'uniform',
                'min': 0.5,
                'max': 1
            },
            'batches_per_decay': {
                'distribution': 'int_uniform',
                'min': 10,
                'max': 50
            },
            'epochs': {
                'distribution': 'int_uniform',
                'min': 10,
                'max': 15
            },
            'early_stopping': {
                'distribution': 'int_uniform',
                'min': 5,
                'max': 10
            },
            'batch_size': {
                'values': [128, 256, 512, 1024]
            },
            'optimizer': {
                'values': ['Adam', 'Nadam']
            }
        }
    }
    return sweep_config


def _sweep_setup(params):
    import wandb
    config = wandb.init().config
    params['ms level'] = ['1', '1a']
    inputs = [1, 1, 1, 1, 1]  # [config.mean_input, config.var_input, config.max_input, config.count_input, config.mz_input]
    params['channels'] = [f for f, ff in zip([0, 1, 2, 3, 4], inputs) if ff]
    params['ms1shape'][-1] = len(params['channels'])

    return config, params


def _sweep_model(params, config):
    from tensorflow.keras.layers import Input, Flatten, Dense, Dropout, Concatenate, SeparableConv2D, MaxPool2D
    from tensorflow.keras.models import Model
    from keras import backend as K

    kernels = (config.kernel_size, config.kernel_size)
    pools = (config.pool_size, config.pool_size)
    input_1 = Input(shape=tuple(params['ms1shape']), name='ms1_input')
    x = Dropout(rate=config.input_dropout)(input_1)
    x = SeparableConv2D(filters=config.kernel_count, kernel_size=kernels)(x)
    x = MaxPool2D(pool_size=pools, strides=pools)(x)
    x = Flatten()(x)
    x = Dense(1024, activation='relu')(x)
    x = Dense(254, activation='relu')(x)

    input_2 = Input(shape=(params['ms1 add shape']), name='ms1_add_input')
    x = Concatenate()([x, input_2])
    x = Dense(254, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    x = Dense(2, activation='softmax')(x)

    model = Model(inputs=[input_1, input_2], outputs=x)
    learning_rate = config.learning_rate
    model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer=eval(f'optimizers.{config.optimizer}')(learning_rate=learning_rate), run_eagerly=True)
    print('Amount of parameters: {:,}'.format(np.sum([K.count_params(w) for w in model.trainable_weights])))

    return model


def _write_prediction(params, model, test_gen):
    mode = 'a' if os.path.exists(f'{params["path"].rsplit("/", 2)[0]}/metadata/{params["_id"]}.txt') else 'w'
    with open(f'{params["path"].rsplit("/", 2)[0]}/metadata/{params["_id"]}.txt', mode) as f:
        init = "\n" if mode == "a" else ""
        f.write(f'{init}{model.evaluate(test_gen, verbose=0)[-1]}')


def _wandb_sweep_train(params, data, class_weight):
    from wandb.integration.keras import WandbCallback
    from tensorflow.keras.callbacks import EarlyStopping
    from network.compile import _get_generators

    # Setting up configurations and Dategenerators
    config, params = _sweep_setup(params)
    train_gen, val_gen, test_gen = _get_generators(params['_id'], data, params)
    model = _sweep_model(params, config)
    n_classes = len(_get_dbs(params["database"], 'filtered').find_one()['classes'])
    len_output = model.layers[-1].output_shape[-1]
    if not ((n_classes == len_output) or (n_classes == 2 and len_output == 1)):
        print('Output dimensions do not match filtered data. Stop sweep and re-filter or change architecture'); sleep(10)

    model.fit(x=train_gen, validation_data=val_gen, epochs=config.epochs, class_weight=class_weight, callbacks=[WandbCallback(), EarlyStopping(monitor='val_accuracy', patience=config.early_stopping)])
    _write_prediction(params, model, test_gen)


def _fetch_model(input_args):
    import wandb
    api = wandb.Api()
    team_name = input_args.team_name if input_args.team_name else wandb.api.default_entity
    try:
        runs = api.sweep(f"{team_name}/{input_args.project_name}/{input_args.sweep_id}").runs
    except:
        quit('Cannot find sweep, make sure configuration file and sweep id is correct.')

    best_or_name = input('Input name of run or "best" for best run? ').lower()
    try:
        if best_or_name in ['best']:
            run = sorted(runs, key=lambda run: run.summary.get("val_accuracy", 0), reverse=True)[0]
        else:
            run = [f for f in runs if f.name == best_or_name][0]
    except:
        quit('Cannot find run, make sure to input the name from the run. example: atomic-red-10')

    if not run.state == 'finished':
        if _validated_input(f'Run state is "{run.state}". Want to download anyway?', ['y', 'n'], default_option='y', equals_to='n'):
            quit('Re-run to get another run!')

    run.file('model-best.h5').download(root=f"{input_args.path}metadata/", replace=True)
    os.rename(f"{input_args.path}metadata/model-best.h5", f"{input_args.path}metadata/{run.name}.h5")
    print(f'{run.name}.h5 saved in the metadata folder!')


def _delete_model_files(input_args):
    import wandb
    api = wandb.Api()
    team_name = input_args.team_name if input_args.team_name else wandb.api.default_entity
    runs = api.sweep(f"{team_name}/{input_args.project_name}/{input_args.sweep_id}").runs
    runs = sorted(runs, key=lambda run: run.summary.get("val_accuracy", 0), reverse=True)

    keep_amount = input('How many (of the best) models do you want to keep?')
    model_files = [g for f in runs[int(keep_amount):] for g in f.files() if 'h5' in g.name]
    [f.delete() for f in model_files]
    quit(f'\n{len(model_files)} models deleted, freeing up {round(sum([f.size * 1e-9 for f in model_files]), 1)} GB')


def _wandb_sweep(input_args):
    import wandb
    if input_args.sweep or input_args.sweep_id:
        team_name = input_args.wandb_team_name if input_args.wandb_team_name else wandb.api.default_entity
        if input_args.sweep_id is None:
            input_args.sweep_id = wandb.sweep(_sweep_config(), project=input_args.wandb_project_name, entity=team_name)

        input_args, name = _set_init(input_args)
        columns = _ms2ai_columns()
        col_query = {f: {'$exists': True} for f in columns}
        data, class_weight, unique_choice, filtered = _get_partitions(input_args, columns, col_query)
        data, shapes = _add_data(input_args, data)
        params, input_args = _get_params(input_args, name, unique_choice, shapes)
        wandb.agent(input_args.sweep_id, project=input_args.wandb_project_name, entity=team_name, function=partial(_wandb_sweep_train, params, data, class_weight))
        quit('Sweep completed, ending!')

    if input_args.fetch:
        _fetch_model(input_args)

    if input_args.delete:
        _delete_model_files(input_args)
