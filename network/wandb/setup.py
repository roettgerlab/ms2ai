import os

from storage.db_init import _get_dbs


def _init(project_name, wandb_params):
    import wandb
    if wandb_params['apply']:
        team_name = wandb_params['team name'] if wandb_params['team name'] else wandb.api.default_entity
        wandb.init(project=wandb_params['project name'], name=project_name, entity=team_name)


def _save_model(model, wandb_params):
    import wandb

    if wandb_params['apply']:
        model.save(os.path.join(wandb.run.dir, "model.h5"))


def _callbacks(input_args, wandb_params, datagens):
    if wandb_params['apply']:
        from wandb.keras import WandbCallback
        return [WandbCallback(labels=_get_dbs(input_args.db_name, 'filtered').find_one({'_id': 'index'})['classes'], training_data=datagens[0], validation_data=datagens[1], predictions=16)]
    else:
        return []


def _confusion_matrix(wandb_params, labels, predictions, label_names):
    import wandb
    if wandb_params['apply']:
        wandb.log({"my_conf_mat_id": wandb.plot.confusion_matrix(preds=predictions, y_true=labels, class_names=list(label_names.values()))})
