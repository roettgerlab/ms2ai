from collections import defaultdict
from itertools import chain

import numpy as np

from storage.db_init import _get_dbs
from utils.common import _validated_input


def _norm_values(db, col, attr, query):
    min = _get_dbs(db, col).find({**{attr: {'$exists': True}}, **query}).sort(attr, 1)[0][attr]
    max = _get_dbs(db, col).find({**{attr: {'$exists': True}}, **query}).sort(attr, -1)[0][attr]
    return {'min': min, 'max': max, 'coll': _get_dbs(db, col)}


def _get_classes(db, col, attr, query):
    return {'labels': _get_dbs(db, col).find({**{attr: {'$exists': True}}, **query}).distinct(attr), 'coll': _get_dbs(db, col)}


def _comp_attr(db, col, attr, query):
    def_attr = attr.replace('*', '').replace('+', '')
    if '*' in attr:
        return _norm_values(db, col, def_attr, query)
    elif '+' in attr:
        return _get_classes(db, col, def_attr, query)
    else:
        return {'coll': _get_dbs(db, col)}


def _comp_index(input_args):
    accs = _get_dbs(input_args.db_name, 'filtered').distinct('accession')
    ids = _get_dbs(input_args.db_name, 'filtered').distinct('identifier')
    attr_dict = {}
    skip_keys = []
    for attr in list(chain(*[input_args.ms1_add_input, input_args.ms2_add_input])):
        def_attr = attr.replace('*', '').replace('+', '')
        if _get_dbs(input_args.db_name, 'filtered').find_one({def_attr: {'$exists': True}}):
            attr_dict[attr] = _comp_attr(input_args.db_name, 'filtered', attr, {})
        elif _get_dbs('ms2ai', 'files').find_one({def_attr: {'$exists': True}, 'identifier': {'$in': ids}}):
            attr_dict[attr] = _comp_attr('ms2ai', 'files', attr, {'identifier': {'$in': ids}})
        elif _get_dbs('ms2ai', 'projects').find_one({def_attr: {'$exists': True}, 'accession': {'$in': accs}}):
            attr_dict[attr] = _comp_attr('ms2ai', 'projects', attr, {'accession': {'$in': accs}})
        elif _validated_input(f'{attr} not found, continue without it?', ['y', 'n'], equals_to='y', default_option='y', show_options=True, force_default=input_args.force_default):
            skip_keys.append(attr)
        else:
            quit()

    return accs, ids, attr_dict, skip_keys


def _comp_data(input_args):
    accs, ids, attr_dict, skip_keys = _comp_index(input_args)
    data = defaultdict(dict)
    for f in _get_dbs(input_args.db_name, 'filtered').find({'_id': {'$ne': 'index'}}):
        query = {'filtered': {}, 'files': {'identifier': {'$in': ids}}, 'projects': {'accession': {'$in': accs}}}
        for g, gg in zip([input_args.ms1_add_input, input_args.ms2_add_input], ['ms1', 'ms2']):
            ms_data = []
            for attr in g:
                if attr in skip_keys:
                    continue
                def_attr = attr.replace('*', '').replace('+', '')
                coll = attr_dict[attr]['coll']
                point = (f if coll.name == 'filtered' else coll.find_one(query[coll.name]))[def_attr]
                if '*' in attr:
                    point = [(point - attr_dict[attr]['min']) / (attr_dict[attr]['max'] - attr_dict[attr]['min'])]
                elif '+' in attr:
                    point = [int(point == f) for f in attr_dict[attr]['labels']]
                else:
                    point = [point]
                ms_data.append(point)
            data[f['_id']][gg] = np.array(list(chain(*ms_data)))

    return data
