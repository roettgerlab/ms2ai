from tensorflow.keras.callbacks import Callback


class AdditionalValidationSets(Callback):
    def __init__(self, data, verbose=0, batch_size=None):
        """
        :param validation_sets:
        a list of 3-tuples (validation_data, validation_targets, validation_set_name)
        or 4-tuples (validation_data, validation_targets, sample_weights, validation_set_name)
        :param verbose:
        verbosity mode, 1 or 0
        :param batch_size:
        batch size to be used when evaluating on the additional datasets
        """
        super(AdditionalValidationSets, self).__init__()
        self.data = data
        self.epoch = []
        self.history = {}
        self.verbose = verbose
        self.batch_size = batch_size

    def on_train_begin(self, logs=None):
        self.epoch = []
        self.history = {}

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epoch.append(epoch)

        for sets in self.data:
            results = self.model.evaluate(sets, verbose=self.verbose, batch_size=self.batch_size)
            for metric, result in zip(self.model.metrics_names, results):
                logs[f'test_{metric}'] = result