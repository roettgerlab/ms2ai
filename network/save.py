import os
from datetime import datetime

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from tensorflow.keras.utils import plot_model

from extractor.common import _import_table
from network.wandb.setup import _save_model


def _plot(input_args, history, params):
    metric = 'accuracy' if params['classification'] else 'loss'
    for f in [f for f in history.history if metric in f]:
        plt.plot(history.history[f])
    title_ext = f' ({len(params["classes"])} classes)' if params['classification'] else ''
    plt.title(f'Validation {metric}{title_ext}')
    plt.ylabel(f'{metric}')
    plt.xlabel('Epoch')
    plt.legend([['Training', 'Validation', 'Testing'][i] for i, _ in enumerate([f for f in history.history if metric in f])], loc='upper left')
    plt.savefig(f'{input_args.path}metadata/{params["_id"]}.png')


def _write_to_file(input_args, history, model, generators):
    if input_args.write_to_file:
        path = f'{input_args.path}metadata/{input_args.test_model}.csv'
        if generators:
            metrics = [[f'{f}{g}' for g in model.metrics_names] for f in ['', 'val_']]
            train_input = [str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")), input_args.db_name, 'train'] + [len(history.epoch)] + model.evaluate(generators[0], verbose=0)
            val_input = [str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")), input_args.db_name, 'validation'] + [0] + model.evaluate(generators[1], verbose=0)
            columns = ['time', 'db', 'type', 'epochs'] + metrics[0]
            df = pd.DataFrame([train_input] + [val_input], columns=columns)
        else:
            input = [str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")), input_args.db_name, 'test'] + [0] + [history[f] for f in model.metrics_names]
            columns = ['time', 'db', 'type', 'epochs'] + model.metrics_names
            df = pd.DataFrame([input], columns=columns)
        if os.path.exists(path):
            df = _import_table(path).append(df, ignore_index=True)
        pd.DataFrame.to_csv(df, path, index=False)


def _print_model(input_args, model, params, wandb_params):
    try:
        if input_args.print_model:
            print(model.summary())
            if os.path.exists(f'{input_args.path}metadata/{params["_id"]}-architecture.png'):
                os.remove(f'{input_args.path}metadata/{params["_id"]}-architecture.png')
            plot_model(model, to_file=f'{input_args.path}metadata/{params["_id"]}-architecture.png', show_shapes=True)
    except:
        print('Graphviz not working (install: https://graphviz.org/ and pip install pydot)')
    if input_args.wandb:
        _save_model(model, wandb_params)


def _inference_dropout_plot(input_args, params, y_real, avg_preds, norm_stds):
    # Remove old plot file
    if os.path.exists(f'{input_args.path}metadata/{params["_id"]}_id_{input_args.inference_dropouts}.png'):
        os.remove(f'{input_args.path}metadata/{params["_id"]}_id_{input_args.inference_dropouts}.png')
    # Setup Plot
    fig, ax = plt.subplots()
    print(f'Plotting Dropout Inferences{"": <50}', end='\r')
    plt.set_cmap('Reds')
    b = ax.scatter(y_real, avg_preds, c=[f for f in norm_stds], s=[4 for f in norm_stds], facecolors='none')
    ax.plot([0, 1], [0, 1], lw=2, color='black', ls='dotted')
    ax.set_xlabel('Real Values')
    ax.set_ylabel('Predicted Values')
    plt.colorbar(b, ax=ax)
    plt.xlim([0, 1])
    plt.tight_layout()
    ax2 = ax.twinx()
    sns.kdeplot(y_real, color='black', ax=ax2, bw_adjust=0.5, linewidth=1.8)
    ax2.get_yaxis().set_visible(False)
    plt.savefig(f'{input_args.path}metadata/{params["_id"]}_{input_args.db_name}_id_{input_args.inference_dropouts}.png')
    plt.cla(); plt.clf()
