import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from network.compile import _get_callbacks, _get_model, _get_generators
from network.dlomix import _intensity_columns, _rt_columns
from network.init import _set_init, _get_params, _ms2ai_columns
from network.partitions import _get_partitions, _add_data
from network.save import _print_model, _write_to_file, _plot
from network.test import _test_model, _get_test_batchsize
from network.wandb.setup import _init


def _run_network(input_args, wandb_params):
    input_args, name = _set_init(input_args)
    columns = _ms2ai_columns() if name.startswith('ms2ai-') else _intensity_columns() if 'int' in name else _rt_columns()
    col_query = {f: {'$exists': True} for f in columns}
    data, class_weight, unique_choice, filtered = _get_partitions(input_args, columns, col_query)
    data, shapes = _add_data(input_args, data)
    params, input_args = _get_params(input_args, name, unique_choice, shapes)

    model = _get_model(input_args, params['_id'], params)
    _print_model(input_args, model, params, wandb_params)
    if input_args.wandb:
        _init(params['_id'], wandb_params)

    if input_args.train_model:
        training_generator, validation_generator, testing_generator = _get_generators(params['_id'], data, params)
        callbacklist = _get_callbacks(input_args, params, [training_generator, validation_generator, testing_generator], wandb_params)
        history = model.fit(x=training_generator, validation_data=validation_generator, epochs=params['epochs'], callbacks=callbacklist,
                  class_weight=class_weight, verbose=input_args.verbose, use_multiprocessing=(True if input_args.workers > 1 else False), workers=input_args.workers)
        _plot(input_args, history, params) if input_args.plot_history else ''
        _write_to_file(input_args, history, model, [training_generator, validation_generator])

    if len(data['testing']['label']) < 10000:
        params = _get_test_batchsize(data['testing']['label'], params)
    testing_generator = _get_generators(name, data, params)[-1]
    model = _get_model(input_args, params['_id'], params, test=True)

    return _test_model(input_args, params, model, testing_generator, data, wandb_params)
