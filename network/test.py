from itertools import chain

import numpy as np
from sklearn.metrics import confusion_matrix

from network.save import _write_to_file, _inference_dropout_plot
from network.wandb.setup import _confusion_matrix
from storage.db_init import _get_dbs
from storage.serialization import _np_save


def _confusion_output(cm, labels, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    [print(f'{f}: {labels[f]}') for f in labels]
    print(f'Confusion matrix ({round((sum([f[i] for i, f in enumerate(cm)]) / sum([sum(f) for f in cm])) * 100, 3)}%):')
    columnwidth = max([len(str(x)) for x in labels.values()] + [5])
    empty_cell = " " * columnwidth
    # Print header
    print("    " + empty_cell * 2 + "Predicted")
    print("    " + empty_cell, end=" ")
    for label in labels.values():
        print("%{0}s".format(columnwidth) % label, end=" ")
    print()
    # Print rows
    for i, label in enumerate(labels.values()):
        print("    %{0}s".format(columnwidth) % label, end=" ")
        for j in range(len(labels)):
            cell = "%{0}.1f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print(cell, end=" ")
        print()


def _get_test_batchsize(test_data, params):
    params['shuffle'] = False
    for i in reversed(range(32, params['batch size'])):
        if len(test_data) % i == 0:
            params['batch size'] = i
            break
    else:
        params['batch size'] = 32

    return params


def _inference_dropout_test(input_args, params, y_real, y_preds):
    print(f'Computing averages{"": <50}', end='\r')
    avg_preds = np.array(list(chain(*np.mean(y_preds, axis=0))))
    std_preds = np.array(list(chain(*np.std(y_preds, axis=0))))

    print(f'Computing reduced permutations{"": <50}', end='\r')
    permutation = np.random.permutation(len(y_real))[:input_args.inference_dropout_size]
    y_real = y_real[permutation]
    avg_preds = avg_preds[permutation]
    std_preds = std_preds[permutation]

    print(f'Removing outliers{"": <50}', end='\r')
    outliers = ((std_preds >= np.percentile(std_preds, 1)) & (std_preds <= np.percentile(std_preds, 99)))
    y_real = list(y_real[outliers])
    avg_preds = list(avg_preds[outliers])
    std_preds = list(std_preds[outliers])

    norm_stds = [f / max(std_preds) for f in std_preds]
    _inference_dropout_plot(input_args, params, y_real, avg_preds, norm_stds)


def _pred_with_print(i, input_args, generator, model):
    print(f'Inference dropout computations: {i + 1} / {input_args.inference_dropouts}{"": <50}', end='\r')
    return model.predict(generator, verbose=input_args.verbose)


def _custom_pred_calc(input_args, data, y_real, y_pred):
    seqs = data['testing']['sequence'][:len(y_pred)]
    write_output = np.array([np.array([seq, real, pred], dtype=object) for (seq, real, pred) in zip(seqs, y_real, y_pred)])
    _np_save(write_output, f'{input_args.path}metadata/pred.txt')


def _test_model(input_args, params, model, generator, data, wandb_params):
    print('Calculating prediction labels', end='\n')
    if input_args.write_to_file:
        y_pred = model.evaluate(generator, verbose=input_args.verbose)
        result = {f: y_pred[i] for i, f in enumerate(model.metrics_names)}
        return _write_to_file(input_args, result, model, [])

    if input_args.inference_dropouts:
        y_real = np.concatenate([y for x, y in generator], axis=0)
        y_preds = [_pred_with_print(i, input_args, generator, model) for i in range(input_args.inference_dropouts)]
        _inference_dropout_test(input_args, params, y_real, y_preds)

    elif params['classification']:
        y_pred = list(model.predict(generator, verbose=input_args.verbose).argmax(axis=-1))
        y_real = data['testing']['label'][:len(y_pred)]
        custom_comps = False
        if custom_comps:
            _custom_pred_calc(input_args, data, y_real, y_pred)

        _confusion_output(confusion_matrix(y_real, y_pred), labels=_get_dbs(input_args.db_name, 'filtered').find_one({'_id': 'index'})['classes'])
        if wandb_params['apply']:
            _confusion_matrix(wandb_params, y_real, y_pred, _get_dbs(input_args.db_name, 'filtered').find_one({'_id': 'index'})['classes'])

    else:
        y_pred = model.evaluate(generator, verbose=input_args.verbose)
        [print(f'{f}: {y_pred[i]}') for i, f in enumerate(['mean squared error', 'mean absolute error'])]
