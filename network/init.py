import random
import string
from datetime import datetime

import numpy as np

from extractor.common import _get_common_ms1shape
from network.dlomix import _attempt_import
from storage.db_init import _get_dbs
from storage.serialization import _create_file


def _ms2ai_columns():
    return {'_id': 1, 'label': 1, 'sequence': 1}


def _set_init(input_args):
    input_args.train_level = [f for f in ['1' if input_args.ms1_input else '', '2' if int(input_args.ms2_input) else '',
                              '1a' if input_args.ms1_add_input else '', '2a' if input_args.ms2_add_input else ''] if f]
    name = input_args.train_model if input_args.train_model else input_args.test_model if input_args.test_model else "sweep"
    name = name if '-' in name else f'{name}-{"".join(random.choice(string.ascii_letters + string.digits) for i in range(10))}'
    input_args.test_model = name
    if name.startswith('ms2ai-') and not input_args.train_level:
        quit('No inputs chosen for the ms2ai model, please specify -1, -2, -1a, or -2a')
    if input_args.wandb:
        _attempt_import()

    _create_file(f'{input_args.path}metadata/')
    if input_args.seed:
        random.seed(input_args.seed)
        np.random.seed(input_args.seed)

    return input_args, name


def _construct_params(input_args, name, binning, filtered, files, unique_choice, shapes):
    params = {'_id': name,
              'ms1shape': _get_common_ms1shape(input_args.db_name)[:-1] + [len(input_args.channels)],
              'ms2shape': [2, binning['bin_amount']] if binning['ms2_binning'] else [2, input_args.spectra_length],
              'binning': binning['ms2_binning'],
              'ms1 add input': input_args.ms1_add_input,
              'ms1 add shape': shapes['ms1'],
              'ms2 add input': input_args.ms2_add_input,
              'ms2 add shape': shapes['ms2'],
              'channels': [{'mean': 0, 'var': 1, 'max': 2, 'count': 3, 'mz': 4}[f] for f in input_args.channels],
              'batch size': input_args.batch_size,
              'classification': filtered.find_one({'_id': 'index'})['classification'],
              'classes': list(filtered.find_one({'_id': 'index'})['classes'].keys()),
              'max mz': max(files.find({'identifier': {'$in': filtered.distinct('identifier')}, "ms1 filter max": {'$exists': True}}).distinct("ms1 filter max")),
              'sequence_length': input_args.sequence_length,
              'ms level': input_args.train_level,
              'ms2_length': input_args.spectra_length,
              'epochs': input_args.epochs,
              'shuffle': True,
              'learning rate': input_args.learning_rate,
              'early stopping': input_args.early_stopping,
              'validation percentage': input_args.validation_split,
              'testing percentage': input_args.test_split,
              'training accession': [] if input_args.identifier_split else unique_choice,
              'training identifier': unique_choice if input_args.identifier_split else [],
              'database': input_args.db_name,
              'path': f'{input_args.path}images/',
              'start time': str(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
              }

    return params


def _get_params(input_args, name, unique_choice, shapes, save_params=True):
    if _get_dbs('ms2ai', 'parameters').find_one(name):
        params = _get_dbs('ms2ai', 'parameters').find_one({'_id': name})
        split_choice = 'identifier' if input_args.identifier_split else 'accession'
        params[f'training {split_choice}'] = list(set(params[f'training {split_choice}'] + unique_choice))
        params['database'] = list(set(([params[f'database']] if not isinstance(params[f'database'], list) else params[f'database']) + [input_args.db_name]))
        input_args.train_level = params['ms level']
        if input_args.train_model:
            _get_dbs('ms2ai', 'parameters').update_one({'_id': name}, {'$set': params})

        if not (shapes['ms1'] == params['ms1 add shape'] and shapes['ms2'] == params['ms2 add shape']):
            quit(f'Current additional inputs do not correspond with previous models parameters. '
                 f'\nPrevious version:\nMS1: {params["ms1 add input"]} Shape: {params["ms1 add shape"]}\nMS2: {params["ms2 add input"]} Shape: {params["ms2 add shape"]}'
                 f'\nCuddent version:\nMS1: {input_args.ms1_add_input}: Shape: {shapes["ms1"]}\nMS2: {input_args.ms2_add_input}: Shape: {shapes["ms2"]}')

        return params, input_args

    else:
        filtered = _get_dbs(input_args.db_name, 'filtered')
        files = _get_dbs('ms2ai', 'files')
        binning = _get_dbs(input_args.db_name, 'peptides').find_one({'_id': 'sizes'})['binning']
        params = _construct_params(input_args, name, binning, filtered, files, unique_choice, shapes)

        if save_params:
            _get_dbs('ms2ai', 'parameters').insert_one(params)
        print(f'\nModel : {name}\nOutput: {filtered.find_one({"_id": "index"})["category"]}\nInputs: {" ".join(input_args.train_level if input_args.train_level else [])}', end='\n')
        return params, input_args
