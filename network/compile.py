import os
from itertools import chain

from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers.schedules import ExponentialDecay

from network.dlomix import _rt_datagens, _int_datagens
from network.extra_validation import AdditionalValidationSets
from network.ms2ai.datagenerator import DataGenerator
from network.ms2ai.model import ms2aiModel
from network.wandb.setup import _callbacks


def _get_callbacks(input_args, params, datagens, wandb_params):
    monitor = 'val_accuracy' if params['classification'] else 'val_loss'
    save_weights = False if input_args.train_model == 'ms2ai' else True
    checkpoint = [ModelCheckpoint(f'{input_args.path}metadata/{params["_id"]}.h5', monitor=monitor, save_best_only=True, save_weights_only=save_weights)]
    test_val = [AdditionalValidationSets([datagens[2]])] if input_args.testing_validation else []
    early_stopping = [EarlyStopping(monitor=monitor, patience=params['early stopping'])] if params['early stopping'] != 0 else []
    tensorboard_callback = TensorBoard(log_dir=f'{input_args.path}metadata/tensorboard/', update_freq='batch') if input_args.tensorboard else []
    callbacks = checkpoint + test_val + early_stopping + tensorboard_callback + _callbacks(input_args, wandb_params, datagens)

    return callbacks


def _compile_model(input_args, params, model):
    metrics = ['accuracy' if params['classification'] else 'mean_absolute_error']
    if not params['_id'].startswith('ms2ai-') and not params['classification']:
        from dlomix.eval.rt_eval import TimeDeltaMetric
        metrics.append(TimeDeltaMetric())

    LR = ExponentialDecay(
            params['learning rate'],
            decay_steps=2000,
            decay_rate=1)

    model.compile(loss='categorical_crossentropy' if params['classification'] else 'mse',
                  metrics=metrics,
                  optimizer=optimizers.Adam(learning_rate=LR), run_eagerly=True)

    if not params['_id'].startswith('ms2ai-'):
        build = (None, input_args.sequence_length) if 'int' not in params['_id'] else {"sequence": (None, input_args.sequence_length), "collision_energy": (None, 1), "precursor_charge": (None, 6)}
        model.build(build)
    return model


def _construct_model(model_name, input_args, params, test):
    if model_name.startswith('ms2ai-'):
        return ms2aiModel(params).get_network()
    else:
        from dlomix.models import deepLC, base, prosit
        from network.mod_prosit import PrositRetentionTimePredictor
        models = {'ms2ai_prosit': PrositRetentionTimePredictor(seq_length=input_args.sequence_length),
                  'prosit_id': PrositRetentionTimePredictor(seq_length=input_args.sequence_length),
                  'prosit': prosit.PrositRetentionTimePredictor(seq_length=input_args.sequence_length),
                  'deeplc': deepLC.DeepLCRetentionTimePredictor(seq_length=input_args.sequence_length),
                  'dlomix': base.RetentionTimePredictor(seq_length=input_args.sequence_length),
                  'prosit_int': prosit.PrositIntensityPredictor(seq_length=input_args.sequence_length)}

        if not model_name.split('-')[0] in models:
            quit(f'{model_name} not understood.')
        if model_name.split('-')[0] == 'prosit' and test and input_args.inference_dropouts:
            return models[f"ms2ai_{model_name.split('-')[0]}"]
        return models[model_name.split('-')[0]]


def _get_model(input_args, model_name, params, test=False):
    model = _construct_model(model_name, input_args, params, test)
    model = _compile_model(input_args, params, model)
    if os.path.exists(f'{input_args.path}/metadata/{model_name}.h5'):
        if params['_id'].startswith('ms2ai-'):
            model = load_model(f'{input_args.path}metadata/{model_name}.h5')
        else:
            model.load_weights(f'{input_args.path}metadata/{model_name}.h5')
    return model


def _get_generators(name, data, params):
    if name.startswith(('ms2ai-', 'sweep-')):
        labels = {dataset: {ids: labels for ids, labels in zip(data[dataset]['_id'], data[dataset]['label'])} for dataset in data}
        partition = {dataset: data[dataset]['_id'] for dataset in data}
        add_data = {ids: {'ms1': ms1, 'ms2': ms2} for ids, ms1, ms2 in
                     zip(list(chain(*[data[dataset]['_id'] for dataset in data])),
                         list(chain(*[data[dataset]['ms1'] for dataset in data])),
                         list(chain(*[data[dataset]['ms2'] for dataset in data])))}
        training_generator = DataGenerator(partition['training'], labels['training'], params, add_data)
        validation_generator = DataGenerator(partition['validation'], labels['validation'], params, add_data)
        testing_generator = DataGenerator(partition['testing'], labels['testing'], params, add_data)
    elif 'int' not in name:
        training_generator, validation_generator, testing_generator = _rt_datagens(params, data)
    else:
        training_generator, validation_generator, testing_generator = _int_datagens(params, data)

    return training_generator, validation_generator, testing_generator
