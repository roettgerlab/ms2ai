def _attempt_import():
    try:
        import dlomix
    except ImportError:
        quit('DLOmix not installed. Without this you wont be able to run dlomix models\n to install run:\n'
             'pip install dlomix')


def _rt_columns():
    return {'_id': 1, 'sequence': 1, 'modified sequence': 1, 'label': 1}


def _rt_datagens(params, data):
    from dlomix.data import RetentionTimeDataset
    # RetentionTimeDataset({'parameters': {'sequence': data['training']['sequence'], 'retention time': data['training']['label']}})
    training_input = RetentionTimeDataset(data_source=tuple([data['training']['sequence'], data['training']['label']]), seq_length=params['sequence_length'],
                                          batch_size=params['batch size'], test=False).train_data if data['training']['label'].size != 0 else None
    validation_input = RetentionTimeDataset(data_source=tuple([data['validation']['sequence'], data['validation']['label']]), seq_length=params['sequence_length'],
                                          batch_size=params['batch size'], test=False).train_data if data['validation']['label'].size != 0 else None
    testing_input = RetentionTimeDataset(data_source=tuple([data['testing']['sequence'], data['testing']['label']]), seq_length=params['sequence_length'],
                                          batch_size=params['batch size'], test=False).train_data if data['testing']['label'].size != 0 else None

    return training_input, validation_input, testing_input


def _intensity_columns():
    return {'_id': 1, 'identifier': 1, 'accession': 1, 'raw file': 1, 'retention time': 1, 'modified sequence': 1, 'used scan': 1, 'charge': 1,
            'intensity': 1, 'fragmentation': 1, 'mass analyser': 1, 'length': 1, 'nce': 1, 'm/z': 1, 'ms/ms m/z': 1, 'label': 1}


def _int_datagens(params, data):
    from dlomix.data import IntensityDataset
    training_input = IntensityDataset(data_source=tuple([data['training']['sequence'], data['training']['label']]), seq_length=params['sequence_length'],
                                          batch_size=params['batch size'], test=False).train_data if data['training']['label'].size != 0 else None
    validation_input = IntensityDataset(data_source=tuple([data['validation']['sequence'], data['validation']['label']]), seq_length=params['sequence_length'],
                                          batch_size=params['batch size'], test=False).train_data if data['validation']['label'].size != 0 else None
    testing_input = IntensityDataset(data_source=tuple([data['testing']['sequence'], data['testing']['label']]), seq_length=params['sequence_length'],
                                          batch_size=params['batch size'], test=False).train_data if data['testing']['label'].size != 0 else None

    return training_input, validation_input, testing_input

