from collections import Counter

import numpy as np

from network.additional import _comp_data
from storage.db_init import _get_dbs


def _get_label_lists(input_data):
    ids = [f[0] for f in input_data]
    labels = [f[1] for f in input_data]
    return ids, labels


def _calc_test_accs(filtered, test_split, input_args, split_choice, seq_query, col_query, columns):
    test_amount = int((test_split / 100) * filtered.count_documents({}))
    all_data = {f['_id']: f['count'] for f in list(filtered.aggregate([{'$sortByCount': f'${split_choice}'}])) if f['_id'] is not None}
    if not all_data:
        quit('No data available')
    shuffled_data = {f: all_data[f] for f in np.random.permutation(list(all_data.keys()))}
    diff_data = [f - test_amount for f in list(np.cumsum(list(shuffled_data.values())))]
    test_data = list(shuffled_data.keys())[:diff_data.index(min(diff_data, key=abs)) + 1] if input_args.test_split else []
    if len(test_data) == len(all_data) and input_args.train_model:
        quit(f'No training data available')
    train_query = {split_choice: {'$nin': test_data}} if test_data else {}

    test_data = list(filtered.find({**{split_choice: {'$in': test_data}}, **seq_query, **col_query}, columns))
    test_data = {f: np.array([g[f] for g in test_data]) for f in list(test_data[0].keys())} if test_data else {f: np.array([]) for f in columns}

    return test_data, train_query


def _split_train_val(input_args, data):
    if not data:
        return {}, {}

    if input_args.split_on_sequence:
        unique_seqs = np.unique(data['sequence'])
        split_amount = int(len(unique_seqs) * (1 - input_args.validation_split / 100))
        permutation = np.random.permutation(range(len(unique_seqs)))
        train = np.isin(data['sequence'], unique_seqs[permutation[:split_amount]])
        validation = {f: data[f][np.logical_not(train)] for f in data}
        train = {f: data[f][train] for f in data}
    else:
        split_amount = int(len(data['label']) * (1 - input_args.validation_split / 100))
        permutation = np.random.permutation(range(len(data['label'])))
        train = {f: data[f][permutation[:split_amount]] for f in data}
        validation = {f: data[f][permutation[split_amount:]] for f in data}

    return train, validation


def _get_train_val(test_query, filtered, input_args, split_choice, seq_query, col_query, columns):
    unique_choice = filtered.find({**{'_id': {'$ne': 'index'}}, **test_query, **seq_query, **col_query}).distinct(split_choice)
    if input_args.train_model or input_args.sweep:
        train_data = list(filtered.find({**{'_id': {'$ne': 'index'}}, **test_query, **seq_query, **col_query}, columns))
        train_data = {f: np.array([g[f] for g in train_data]) for f in list(train_data[0].keys())}
        train_data, validation_data = _split_train_val(input_args, train_data)
        return train_data, validation_data, unique_choice
    else:
        return {'label': np.array([]), '_id': np.array([])}, {'label': np.array([]), '_id': np.array([])}, unique_choice


def _get_class_weights(input_args, train_data):
    if _get_dbs(input_args.db_name, 'filtered').find_one({'_id': 'index'})['classification']:
        counter = dict(Counter(train_data['label']))
        class_weight = {k: v / max(counter.values()) for k, v in counter.items()}
        print(f'Training class weights: {class_weight}')
    else:
        class_weight = {}

    return class_weight


def _db_init(input_args):
    peptides, filtered, filtered_shape = _get_dbs(input_args.db_name, 'peptides', 'filtered', 'filtered shape')
    binning = peptides.find_one({'_id': 'sizes'})['binning']
    if not binning['ms2_binning'] and '2' in input_args.train_level:
        filtered_shape.drop()
        filtered_shape.insert_many(filtered.find({'_id': {'$ne': 'index'}, 'ms2 shape.1': {'$gte': input_args.spectra_length}}))
        print(f'{filtered.count_documents({}) - filtered_shape.count_documents({})} peptides removed due to MS2 length requirements')
        filtered = filtered_shape

    return filtered


def _get_partitions(input_args, columns, col_query):
    filtered = _db_init(input_args)
    seq_query = {"$expr": {"$lte": [{"$strLenCP": "sequence"}, input_args.sequence_length]}} if input_args.train_model != 'ms2ai' else {}
    split_choice = 'identifier' if input_args.identifier_split else 'accession'
    test_data, inv_test_query = _calc_test_accs(filtered, input_args.test_split, input_args, split_choice, seq_query, col_query, columns)
    train_data, validation_data, unique_choice = _get_train_val(inv_test_query, filtered, input_args, split_choice, seq_query, col_query, columns)
    data = {'training': train_data, 'validation': validation_data, 'testing': test_data}
    class_weight = _get_class_weights(input_args, train_data) if (input_args.train_model or input_args.sweep) else {}

    data_count = filtered.count_documents({**{"_id": {"$ne": "index"}}, **seq_query, **{f: {'$exists': True} for f in columns}})
    [print(f'{f} datapoints: {len(data[f]["label"])} ({round(len(data[f]["label"]) / data_count * 100, 1)}%)') for f in data]
    return data, class_weight, unique_choice, filtered


def _add_data(input_args, data):
    add_data = _comp_data(input_args)
    add_data = [[add_data[ids] for ids in data[dataset]['_id']] for dataset in data]
    [data[dataset].update({ms: [f[ms] for f in add]}) for dataset, add in zip(data, add_data) for ms in ['ms1', 'ms2']]
    db = [f for f in ['training', 'validation', 'testing'] if data[f]['ms1']][0]
    shapes = {'ms1': len(data[db]['ms1'][0]), 'ms2': len(data[db]['ms2'][0])}
    return data, shapes
