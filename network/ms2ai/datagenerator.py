from time import time

import numpy as np
from tensorflow.keras.utils import Sequence, to_categorical

from storage.serialization import _np_load


class DataGenerator(Sequence):
    'Generates data for Keras'

    def __init__(self, list_IDs, labels, params, add_data):
        'Initialization'
        self.params = params
        self.add_data = add_data
        self.list_IDs = list_IDs
        self.labels = labels
        self.on_epoch_end()
        self.n = 0
        self.max = self.__len__()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.params['batch size']))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index * self.params['batch size']:(index + 1) * self.params['batch size']]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]
        # Generate data
        X, y = self.__data_generation(list_IDs_temp)
        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.params['shuffle']:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples'
        # Initialize empty sequences
        y = np.zeros(self.params['batch size'])

        default_inputs = {
            '1': np.zeros(tuple([self.params['batch size']] + self.params['ms1shape'])),
            '1a': np.zeros((self.params['batch size'], self.params['ms1 add shape'])),
            '2': np.zeros(tuple([self.params['batch size']] + self.params['ms2shape'])),
            '2a': np.zeros((self.params['batch size'], self.params['ms2 add shape']))
        }

        data_input = {}
        for f in self.params['ms level']:
            data_input[f] = default_inputs[f]

        start = time()
        for i, ID in enumerate(list_IDs_temp):
            try:
                if any(item in ['1', '2'] for item in self.params['ms level']):
                    data = _np_load(f'{self.params["path"]}{ID}.txt')
                    if '1' in self.params['ms level']:
                        data_input['1'][i] = data[0][:, :, self.params['channels']]
                        if 4 in self.params['channels']:
                            data_input['1'][i, :, :, -1] = data_input['1'][i, :, :, -1] / self.params['max mz']
                    if '2' in self.params['ms level']:
                        data_input['2'][i] = data[1] if self.params['binning'] else data[1][1][:, data['data'][1][0][data['data'][1][1].argsort()[-self.params['ms2shape'][1]:]].argsort()]
                if '1a' in self.params['ms level']:
                    data_input['1a'][i] = self.add_data[ID]['ms1']
                if '2a' in self.params['ms level']:
                    data_input['2a'][i] = self.add_data[ID]['ms2']

                y[i] = self.labels[ID]
            except Exception as e:
                quit(f'{ID}: {e}')
        # print(f'Batch load time: {time() - start}')

        try:
            y = to_categorical(y, num_classes=len(self.params['classes'])) if self.params['classification'] else y
        except Exception as e:
            quit(f'Error in attempting one-hot encoding. Most likely batch size or test split issue: {e}')

        return list(data_input.values()), y

    def __next__(self):
        if self.n >= self.max:
            self.n = 0
        result = self.__getitem__(self.n)
        self.n += 1
        return result
