from tensorflow.keras.layers import Dense, Input, Concatenate, Flatten, Conv2D, MaxPool2D, Dropout
from tensorflow.keras.models import Model

class ms2aiModel:
    def __init__(self, params):
        self.params = params

    # First layers
    def phase1_ms1(self):
        input_l = Input(shape=tuple(self.params['ms1shape']), name='ms1_input')
        x_i = Dropout(0.3)(input_l)
        x_i = Flatten(name='ms1_input_flattening')(x_i)

        x = Dense(2048, activation='relu')(x_i)
        x = Dense(1024, activation='relu')(x)
        x = Dense(512, activation='relu')(x)

        x = Concatenate()([x_i, x])
        x = Dense(2048, activation='relu')(x)
        x = Dense(1024, activation='relu')(x)

        return input_l, x

    def phase1_ms2(self):
        # input_l = Input(shape=(self.ms2size[0], 1) if get_config()['extractor']['ms2_binning']['apply'] else (None, 2), name='ms2_lstm_input')  # (Seq_len, 2) 2:[m/z, int]
        # x = LSTM(128)(input_l)
        input_l = Input(shape=tuple(self.params['ms2shape']), name='ms2_input')
        x = Flatten(name='ms2_flatten')(input_l)
        x = Dense(512, activation='relu', name='ms2_dense_1')(x)
        x = Dense(256, activation='relu', name='ms2_dense_2')(x)

        return input_l, x

    def phase1_add_ms1_info(self):
        input_l = Input(shape=(self.params['ms1 add shape']), name='ms1+_input')
        x = Dense(128, activation='relu', name='add_ms1_dense_1')(input_l)

        return input_l, x

    def phase1_add_ms2_info(self):
        input_l = Input(shape=(self.params['ms2 add shape']), name='ms2+_input')
        x = Dense(128, activation='relu', name='add_ms2_dense_1')(input_l)

        return input_l, x

    # Second layers
    def phase2_comb(self):
        network_output = Dense(128, activation='relu', name='comb_input')(self.p1_output_layer)
        network_output = Dense(128, activation='relu', name='comb_output')(network_output)
        return network_output

    # Output layers
    def phase3_comb(self):
        return Dense(len(self.params['classes']), activation='softmax' if self.params['classification'] else 'linear', name='output')

    # Combining the Network
    def get_network(self):
        data = {
            '1': self.phase1_ms1(),
            '1a': self.phase1_add_ms1_info(),
            '2': self.phase1_ms2(),
            '2a': self.phase1_add_ms2_info()
        }

        model_dict = {}
        for f in self.params['ms level']:
            model_dict[f] = data[f]

        if model_dict:
            p1_input_layer = [f[0] for f in list(model_dict.values())]
            p1_outputs = [f[1] for f in list(model_dict.values())]
            self.p1_output_layer = Concatenate()(p1_outputs) if len(p1_outputs) > 1 else p1_outputs[0]
            p2_output_layer = self.phase2_comb()
            p3_output_layer = self.phase3_comb()(p2_output_layer)
            model = Model(inputs=p1_input_layer, outputs=p3_output_layer)

            return model
        else:
            return
