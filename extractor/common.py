import bisect
import shutil
from glob import glob

import pandas as pd
from pymongo import MongoClient

from storage.db_init import _get_dbs
from utils.common import _validated_input


def _import_table(file, memory_file=False):
    # Reading with csv, if that doesn't work, rewind the iterable and try a csv delimiter
    df = pd.read_csv(file, sep='\t', low_memory=False, on_bad_lines='skip')
    if len(df.columns) == 1:
        if memory_file:
            file.seek(0)
        df = pd.read_csv(file, sep=',', low_memory=False, on_bad_lines='skip')
    return df


def _get_lower_bound(list, value):
    if value == list[-1]:
        value = value - 0.000001
    return bisect.bisect(list, value) - 1


def _get_common_ms1shape(db_name):
    aggregate_search = [{"$group":
                             {"_id":
                                  {"category": "$ms1 shape", "name": "$name"},
                              "count": {"$sum": 1}}},
                        {"$sort": {
                            "_id.category": 1,
                            "count": -1}}]
    return list(_get_dbs(db_name, 'peptides').aggregate(aggregate_search))[1]['_id']['category']


def _get_ms1_shape(db_name):
    peptides = _get_dbs(db_name, 'peptides')
    ms_shape = list(peptides.find_one({'_id': 'sizes'}).values())[1:]
    channels = 5 if not peptides.find_one({'ms1 shape': {'$exists': True}}) else peptides.find_one({'ms1 shape': {'$exists': True}})['ms1 shape'][-1]
    return [int(ms_shape[3] / ms_shape[1] * 2 + 1), int(ms_shape[2] / ms_shape[0] * 2 + 1), channels]


def _reset_data(input_args):
    print(f'\nDatabase: {input_args.db_name.split(".")[0]}')
    if input_args.db_name not in MongoClient().list_database_names():
        return print(f'No peptides found in: {input_args.db_name}')

    print('{:,}'.format(len(glob(f'{input_args.path}images/{input_args.db_name.split(".")[0]}/*'))), 'projects')
    print('{:,}'.format(len(glob(f'{input_args.path}images/{input_args.db_name.split(".")[0]}/*/*'))), 'raw files')
    pep_dbs = [f for f in MongoClient()[input_args.db_name].list_collection_names() if f in ['peptides', 'filtered']]
    highest_count = max([MongoClient()[input_args.db_name].command("collstats", f)["count"] for f in pep_dbs] + [0])
    print('{:,}'.format(highest_count), 'images')

    if _validated_input('Sure you want to reset peptide data?', ['y', 'n'], equals_to='y', default_option='y', force_default=input_args.force_default):
        shutil.rmtree(f'{input_args.path}images/{input_args.db_name}', ignore_errors=True)  # Once to remove files
        shutil.rmtree(f'{input_args.path}images/{input_args.db_name}', ignore_errors=True)  # Twice to remove directories. This is needed on WinOS... for some reason
        _get_dbs(input_args.db_name, 'peptides').drop()
        _get_dbs(input_args.db_name, 'filtered').drop()
