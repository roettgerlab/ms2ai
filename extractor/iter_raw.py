from pymongo.errors import BulkWriteError

from extractor.file_metadata import _handle_metadata
from extractor.imgs.create import _peptides, _create_ms2, _create_ms1
from pride_api.common import _file_downloader
from storage.db_init import _get_dbs
from utils.log import _ms2ai_log


def _save_peptides(input_args, peptides, accnr, filename, ms2, multiprocessing):
    if len(peptides) == 1:
        return
    if input_args.metadata or input_args.maxquant_only:
        [f.update({'_id': f'{input_args.db_name}/{f["identifier"]}/{f["pep_id"]}'}) for f in peptides]
        try:
            _get_dbs(input_args.db_name, 'peptides').insert_many(peptides, ordered=False, bypass_document_validation=True)
        except BulkWriteError as e:
            print(f"{accnr}/{filename}: {len(e.details['writeErrors'])} duplicated database entries skipped{'': <50}", end='\r') if not multiprocessing else ''
    else:
        if input_args.extraction_level:
            _peptides(input_args, accnr, filename, peptides, multiprocessing, ms2)


def _skip(input_args, filename, accnr):
    raw_skip = (bool(input_args.raw_files) and filename not in input_args.raw_files)
    skip_skip = (filename in input_args.skip_raw_files if isinstance(input_args.skip_raw_files, list)
                 else filename in input_args.skip_raw_files[accnr])
    return raw_skip or skip_skip


def _iterate_rawfiles(file_link, input_args, zipfile, accnr, multiprocessing):
    try:
        filename, filelink = file_link
        if _skip(input_args, filename, accnr):
            return
        print(f'{accnr}/{filename}: Initializing{"": <50}', end='\r') if not multiprocessing else ''
        filepath = f'{input_args.path}{accnr}/{filename}/'

        if filelink and not input_args.maxquant_only:
            _file_downloader(f'{input_args.path}{accnr}/{filename}', accnr, file_link[1], multiprocessing, f'file.raw')
            if input_args.download:
                return print(f'{accnr}/{filename}: File download complete!{"": <50}', end='\n') if not multiprocessing else ''

        raw_file = None
        if not input_args.maxquant_only:
            from fisher_py import RawFile
            raw_file = RawFile(f'{filepath}file.raw')

        ms2 = _create_ms2(input_args, raw_file, filepath, multiprocessing, accnr, filename) if not input_args.maxquant_only else None
        peptides = _handle_metadata(input_args, filepath, accnr, filename, raw_file, ms2)
        _create_ms1(input_args, raw_file, accnr, filename, multiprocessing) if not (input_args.metadata or input_args.maxquant_only) else ''
        _save_peptides(input_args, peptides, accnr, filename, ms2, multiprocessing)
        del raw_file
        return print(f'{accnr}/{filename}: ✔{"": <50}', end='\n') if not multiprocessing else ''

    except (KeyboardInterrupt, OSError) as e:
        quit(e)
    except Exception as e:
        _ms2ai_log(f'{accnr}/{filename} ({zipfile}) (Raw error), {e.__class__.__name__}: {e}', 'info')
        pass
