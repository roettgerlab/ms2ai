import os
import re
from collections import defaultdict
from functools import partial
from glob import glob
from itertools import chain
from multiprocessing.pool import Pool
from time import time
from zipfile import ZipFile

from extractor.common import _reset_data
from extractor.file_metadata import _maxquant, _match_files
from extractor.filters import _recreate_file_filter
from extractor.init_local import _local_peptide_extractor, _loose_to_zip
from extractor.init_pride import _peptide_extractor
from pride_api.common import _pride_projects
from pride_api.get_urls import _get_project
from scraper.run import _run_scraper
from storage.compressions import _ext_from_zip
from storage.db_init import _get_dbs, _insert_or_update
from utils.common import _validated_input


def _db_init(input_args):
    mz_bin, rt_bin = input_args.mz_bin, input_args.rt_bin
    mz_interval, rt_interval = input_args.mz_interval, input_args.rt_interval
    binning = {"ms2_binning": input_args.ms2_binning, "remove_int_info": input_args.keep_intensities,
               "variable_mz": input_args.variable_mz, "fixed_mz_length": input_args.fixed_mz_length, "bin_amount": input_args.bin_amount}

    pep_db = _get_dbs(input_args.db_name, 'peptides')
    if input_args.inputs and '\\' in input_args.inputs[0]:
        quit('All paths must be formatted using "/" instead of "\\"')

    if os.listdir(f'{input_args.path}temp/') and _validated_input('Remove all files in temp?', valid_values=['y', 'n'], equals_to='y', default_option='y', force_default=input_args.force_default):
        [os.remove(f'{input_args.path}temp/{f}') for f in os.listdir(f'{input_args.path}temp/')]

    if (pep_db.find_one({'_id': 'sizes'}) and pep_db.find_one({'_id': {'$ne': 'sizes'}}) is not None):
        if list(pep_db.find_one({'_id': 'sizes'}).values())[1:] == [mz_bin, rt_bin, mz_interval, rt_interval, binning, input_args.rt_as_percentage] \
                or input_args.maxquant_only \
                or _validated_input('Data of different dimensions already created, want to change dimensions to the one currently saved?', valid_values=['y', 'n'], equals_to='y', default_option='y', force_default=input_args.force_default):
            return
    else:
        _insert_or_update(pep_db, {'_id': 'sizes', 'mz_bin': mz_bin, 'rt_bin': rt_bin, 'mz_interval': mz_interval, 'rt_interval': rt_interval, 'binning': binning, 'rt_as_percentage': input_args.rt_as_percentage}, {'_id': 'sizes'})


def _pride_project(input_args, accessions):
    input_args.accession = [f for f in input_args.inputs if not _get_dbs('ms2ai', 'projects').find_one({'accession': f}) and 'status' not in _get_project(f)]
    if input_args.accession:
        print(f'Fetching PRIDE metadata{"": <50}', end='\r')
        _run_scraper(input_args)
    input_args, accessions = _extractor_filter(input_args, accessions)

    if input_args.threads != 1 and len(accessions) > 1:
        print(f'Starting multi threaded extraction{"": <50}', end='\r')
        with Pool(min(len(accessions), input_args.threads)) as ps:
            for i, _ in enumerate(ps.imap_unordered(partial(_peptide_extractor, input_args=input_args, multiprocessing=True), accessions)):
                print(f'{i + 1} / {len(accessions)}{"": <50}', end='\r')
    else:
        print(f'Starting single threaded extraction{"": <50}', end='\r')
        [_peptide_extractor(accession, input_args, False) for accession in accessions]


def _extractor_query(input_args):
    projects, files = _get_dbs('ms2ai', 'projects', 'files')
    queries = {}
    operator_dict = {'=': '$eq', '!=': '$ne', '<': '$lt', '<=': '$lte', '>': '$gt', '>=': '$gte', 'in': '$in', 'nin': '$nin', 'regex': '$regex', 'exists': '$exists'}
    for query in input_args.query:
        key, value = re.split('|'.join(list([f' {f} ' for f in operator_dict.keys()])), query)  # Intelligent splitting
        operator = operator_dict[re.findall('|'.join(list([f' {f} ' for f in operator_dict.keys()])) + '|$', query)[0].strip()]
        collection = projects if projects.find_one({key: {'$exists': True}}) else files if files.find_one({key: {'$exists': True}}) else None
        if collection is None:
            print(f'{key} not found in any database, skipping!{"": <50}')
            continue
        value = value.split(' ') if operator in ['$in', '$nin'] else int(value) if operator == '$exists' else value
        value_type = list if operator in ['$in', '$nin'] else bool if operator == '$exists' else type(collection.find_one({key: {'$exists': True}})[key])
        queries.update({key: {operator: re.escape(value_type(value)) if operator == '$regex' else value_type(value)}})
    return queries


def _extractor_filter(input_args, accessions=None):
    projects, files = _get_dbs('ms2ai', 'projects', 'files')
    query = _extractor_query(input_args)
    if accessions is not None:
        completed_accs = []
        if input_args.skip_existing:
            print(f'Computing skipping accession{"": <50}', end='\r')
            existing_peps = defaultdict(list)
            [existing_peps[f.split('/')[0]].append(f.split('/')[1]) for f in _get_dbs(input_args.db_name, 'peptides').distinct('identifier')]
            input_args.skip_raw_files = existing_peps if not input_args.skip_raw_files else input_args.skip_raw_files + list(chain(*existing_peps.values()))
            completed_accs = [f for f in existing_peps if len(existing_peps[f]) == _get_dbs('ms2ai', 'projects').find_one({'accession': f})['raw files']]

        queried_accessions = [f for f in accessions if f not in completed_accs and projects.find_one(query)]
        if len(accessions) != len(queried_accessions):
            print(f"Removed accessions: {', '.join(list(set(accessions) - set(queried_accessions)))}{'': <50}", end="\r")
        return input_args, queried_accessions
    else:
        existing_peps = _get_dbs(input_args.db_name, 'peptides').distinct('identifier') if input_args.skip_existing else []
        query_files = defaultdict(list)
        [query_files[f.split('/')[0]].append(f.split('/')[1]) for f in files.find({**query, **{'identifier': {'$nin': existing_peps}}}).distinct('identifier')]
        return input_args, query_files


def _rec_flatten(lst):
    result = []
    for i in lst:
        if not isinstance(i, list):
            result.append(i)
        else:
            result.extend(_rec_flatten(i))

    return result


def _set_files_to_directory(input_args, path):
    if bool([f for f in os.listdir(path) if f.endswith(('.zip', '.raw'))]):
        if os.path.exists(f'{path}working.zip'):
            os.remove(f'{path}working.zip')
        zip_files = [file for file in os.listdir(path) if file.endswith('zip')]
        if input_args.maxquant_file in os.listdir(path):
            zip_files = zip_files + _loose_to_zip(path)
        mq_files = _rec_flatten([_ext_from_zip([input_args.maxquant_file], False, zip_file=ZipFile(f'{path}{f}')) for f in zip_files])
        mq_df, rawfiles = _maxquant(mq_files)
        accnr = path.split('/')[-2 if input_args.path in path else -3]
        _match_files(input_args, rawfiles, rawfiles, mq_df, accnr)
        [os.rename(f'{path}{os.path.basename(f)}', f'{path}{os.path.basename(f).split(".")[0]}/file.raw') for f in glob(f'{path}*.raw')]


def _find_directories(input_args, path):
    print(f'Initiating files{"": <50}', end='\r')
    files = _get_dbs('ms2ai', 'files')
    storage_files = list(set([f.replace('\\', '/').rsplit('/',1)[0] for f in glob(f'{path}**/*.raw' if not input_args.maxquant_only else
                                                                              f'{path}**/{input_args.maxquant_file}', recursive=True)]))
    file_ids = ['/'.join(f.split('/')[-2:]) for f in storage_files]
    input_args, query_files = _extractor_filter(input_args) if (input_args.query or input_args.skip_existing) else (input_args, False)
    print(f'Computing files{"": <50}', end='\r')
    file_paths = [f'{f}/' for i, (f, ids) in enumerate(zip(storage_files, file_ids)) if
                  (ids.split('/')[-1] in query_files[ids.split('/')[0]] if query_files else True) and
                  (_recreate_file_filter(input_args, path, ids, files, len(storage_files), i) if input_args.re_create else True)]
    print(f'Total files: {len(file_paths)}{"": <50}')
    return file_paths


def _local_data(input_args, paths):
    [_set_files_to_directory(input_args, path=f) for f in paths]
    paths = list(set(chain(*[_find_directories(input_args, path=f) for f in paths])))
    if input_args.threads != 1 and len(paths) > 1:
        with Pool(min(len(paths), input_args.threads)) as ps:
            for i, _ in enumerate(ps.imap_unordered(partial(_local_peptide_extractor, input_args=input_args, multiprocessing=True), paths)):
                print(f'{i + 1} / {len(paths)}', end='\r')
    else:
        [_local_peptide_extractor(f, input_args, False) for f in paths]


def _run_extractor(input_args):
    if not os.path.exists(input_args.path):
        quit('Path given in config.json doesn\'t exist. Please specify working path.')

    timer = time()
    if input_args.reset:
        _reset_data(input_args)

    if input_args.inputs or input_args.pride or input_args.re_create:
        _db_init(input_args)
        local_projects = ([f'{f}{"/" if not f.endswith("/") else ""}' for f in input_args.inputs if os.path.isdir(f)] if input_args.inputs else []) + ([input_args.path] if input_args.re_create else [])
        _local_data(input_args, local_projects)

        pride_projects = list(set(([f for f in input_args.inputs if 'status' not in _get_project(f)] if input_args.inputs else []) + (_pride_projects(input_args) if input_args.pride else [])))
        _pride_project(input_args, pride_projects)

    print(f'\nTotal time spent extracting: {int((time() - timer) / 60) } min.', end='\n')
