import os

from extractor.imgs.computations import _get_resolution


def _recreate_file_filter(input_args, path, id, files, max_i, i):
    print(f'Computing files: {i+1} / {max_i}{"": <50}', end='\r')
    try:
        return True if (input_args.metadata or input_args.maxquant_only) else os.path.exists(f"{path}{id}/{'x'.join([str(f) for f in _get_resolution(input_args, files.find_one({'identifier': id})).values()])}.txt")
    except Exception as e:
        return False


def _directory_filter(path, directory):
    contains_exts = [g.split('.')[-1] for g in os.listdir(f'{path}{directory}/') if g.endswith(('raw', 'zip', 'txt'))]
    return len(set(contains_exts)) > 1


def _skip_existing_pepfiles(path):
    return os.path.exists(path)


def _skip_outofbounds(mz_window, rt_window, ranges):
    return mz_window['lower'] < 0 or mz_window['upper'] > len(ranges['mz']) or \
           rt_window['lower'] < 0 or rt_window['upper'] > len(ranges['rt'])


def _skip_custom(peptide):
    return peptide['precursor m/z difference'] > 2

