import os
from functools import partial
from multiprocessing import Pool

from extractor.iter_zip import _pride_iter
from extractor.xml_parser import _xml_parser, _loose_xmls
from pride_api.get_urls import _get_zip_files, _get_raw_files, _loose_MQs, _get_files_api
from storage.db_init import _get_dbs
from storage.serialization import _create_file
from utils.log import _ms2ai_log


def _check_pre_info(input_args, accnr, rawfiles):
    rawfiles = [(file.split("/")[-1].split(".")[0], size) for file, size in rawfiles]
    project_dir = f'{input_args.path}{accnr}/'
    # Check for MaxQuant files
    mq = all([os.path.exists(f'{project_dir}{file}/{input_args.maxquant_file}') for file, size in rawfiles])
    # Remove incomplete and check for raw files
    [os.remove(f'{project_dir}{file}/file.raw') for file, size in rawfiles if os.path.exists(f'{project_dir}{file}/file.raw') and os.path.getsize(f'{project_dir}{file}/file.raw') != size]
    raw = all([os.path.exists(f'{project_dir}{file}/file.raw') for file, size in rawfiles])
    db = _get_dbs('ms2ai', 'files').count_documents({'accession': accnr, 'raw file': {'$in': [f[0] for f in rawfiles]}}) == len(rawfiles)

    return mq and raw and db


def _check_complete_internal(input_args, accnr, all_raw, multiprocessing):
    ids = ['/'.join(f.split('/')[-2:]).split('.')[0] for f in all_raw]
    peptide_check = _get_dbs(input_args.db_name, 'peptides').find({'identifier': {'$in': ids}}).distinct('identifier')
    files_check = _get_dbs('ms2ai', 'files').find({'identifier': {'$in': ids}}).distinct('identifier')
    min_common = min(len(peptide_check), len(files_check))
    print(f'Finished {accnr}. {min_common}/{len(all_raw)} files{"": <50}', end='\n') if not multiprocessing else ''


def _peptide_extractor(accnr, input_args, multiprocessing):
    print(f'\nAccessions: {accnr}{"": <50}', end='\n') if not multiprocessing else ''
    _create_file(f'{input_args.path}{accnr}/')
    try:
        # Find all zipfiles, rawfiles, and loose MaxQuant files.
        zipfiles, rawfiles = _get_zip_files(input_args, _get_files_api(accnr)), _get_raw_files(_get_files_api(accnr), True)
        zipfiles = zipfiles + _loose_MQs(_get_files_api(accnr), input_args, multiprocessing, to_zip=True)
        # Get loose XML files into 'files' db and Check for all MaxQuant files and check for skipability
        _xml_parser(_loose_xmls(accnr, input_args, multiprocessing), accnr, [])
        pre_acquired = _check_pre_info(input_args, accnr, rawfiles)
        # Go through all zipfiles
        if not multiprocessing and input_args.threads > 1:
            with Pool(min(len(zipfiles), input_args.threads)) as ps:
                for i, _ in enumerate(ps.imap_unordered(partial(_pride_iter, input_args=input_args, pre_acquired=pre_acquired, accnr=accnr, rawfiles=[f[0] for f in rawfiles], multiprocessing=True), zipfiles)):
                    print(f'{i + 1} / {len(zipfiles)}', end='\r')
        else:
            [_pride_iter(f, input_args, pre_acquired, accnr, [f[0] for f in rawfiles], multiprocessing) for f in zipfiles]

        # Check to see how many files got extracted properly
        return _check_complete_internal(input_args, accnr, [file[0] for file in rawfiles], multiprocessing)
    except KeyboardInterrupt:
        quit()
    except Exception as e:
        _ms2ai_log(f'{accnr}, {e.__class__.__name__}: {e}', 'info')
        pass
