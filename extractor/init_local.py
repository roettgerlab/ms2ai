import os
from collections import Counter
from functools import partial
from multiprocessing import Pool
from zipfile import ZipFile

from extractor.file_metadata import _maxquant
from extractor.iter_zip import _local_iter
from pride_api.keys import _maxquant_file_names, _pride_keys
from storage.compressions import _ext_from_zip
from storage.db_init import _insert_or_update, _get_dbs
from utils.common import _validated_input
from utils.log import _ms2ai_log


def _loose_to_zip(dirpath):
    if os.path.exists(f'{dirpath}working.zip'):
        os.remove(f'{dirpath}working.zip')
    with ZipFile(f'{dirpath}working.zip', 'w') as zip_file:
        maxquant_files = [f for f in os.listdir(f'{dirpath}') if any([g in f for g in [f.split('.')[0] for f in _maxquant_file_names()]]) and f.endswith(('.xml', '.txt'))]
        [zip_file.write(dirpath + f, f) for f in maxquant_files]

    return ['working.zip']


def _new_metadata(input_args, accnr):
    pride_keys = ['accession'] + _pride_keys()
    print('\nFor easy compatibility with other metadata, please provide exact string matching entries corresponding with the PRIDE standard values.\n'
          'Input values for the project below. Leave entry empty if non-applicable')
    manual_input = {}
    for f in pride_keys:
        key_input = input(f'Project {f if f != "accession" else "name (accession)"} ("?" for examples from other PRIDE projects)')
        if key_input == '?':
            while key_input == '?':
                print(', '.join([g[0] for g in Counter([g[f] for g in _get_dbs('ms2ai', 'projects').find()]).most_common()][0:10]))
                key_input = input(f'Project {f if f != "accession" else "name (accession)"} ("?" for examples from other PRIDE projects)')
        else:
            manual_input[f] = None if key_input == '' and f != 'accession' else key_input

    if manual_input['accession'] == '':
        manual_input['accession'] = accnr
    manual_input['source'] = 'manual input'

    rename = True
    while _get_dbs('ms2ai', 'projects').find_one({'accession': manual_input['accession']}) and rename:
        rename = _validated_input(f'Project name "{manual_input["accession"]}" already exists, overwrite or rename?', valid_values=['overwrite', 'rename'], equals_to='rename', default_option='rename', force_default=input_args.force_default)
        if rename:
            manual_input['accession'] = input('Input new project name (accession): ')

    _insert_or_update(_get_dbs('ms2ai', 'projects'), manual_input, ['accession'])
    return manual_input['accession']


def _new_accnr(input_args, default_option='rename'):
    accnr = input('Input new project name: ')
    pre_existing = bool(_get_dbs('ms2ai', 'projects').find_one({'accession': accnr}))
    while pre_existing:
        name_option = _validated_input(f'Directory with project name "{accnr}" already exists, use or rename?: ', valid_values=['use', 'rename'], equals_to='rename', default_option=default_option, force_default=input_args.force_default)
        if name_option:
            accnr = input('Input new project name: ')
            pre_existing = bool(_get_dbs('ms2ai', 'projects').find_one({'accession': accnr}))
        else:
            pre_existing = False

    return accnr


def _metadata_handler(input_args, accnr):
    if _validated_input('Provide metadata for the project? (Applies to files in project) ', valid_values=['y', 'n'], equals_to='y', default_option='n', force_default=input_args.force_default):
        accnr = _new_metadata(input_args, accnr)

    elif _validated_input(f'Change project name? (Current: {accnr})? ', valid_values=['y', 'n'], equals_to='y', default_option='n', force_default=input_args.force_default):
        accnr = _new_accnr(input_args)

    return accnr


def _get_accnr(input_args, dirpath, zip_files, multiprocessing):
    with ZipFile(f'{dirpath}{zip_files[0]}', 'r') as zip_file:
        mq_file = _ext_from_zip([input_args.maxquant_file], multiprocessing, zip_file=zip_file)[0]
        files = _maxquant(mq_file)[1]
    accnr = dirpath.split('/')[-2 if len(files) == 1 and files[0] != dirpath.split('/')[-2] else -3]

    # Allow the user to provide metadata for the project, if the project is NOT in the project folder and already has information attached
    if not _get_dbs('ms2ai', 'projects').find_one({'accession': accnr}) and not multiprocessing:
        accnr = _metadata_handler(input_args, accnr)

    return accnr


def _local_peptide_extractor(dirpath, input_args, multiprocessing):
    try:
        # Find zipfiles or create a zipfile with the loose files in the project
        if os.path.exists(f'{dirpath}working.zip'):
            os.remove(f'{dirpath}working.zip')
        zip_files = [file for file in os.listdir(dirpath) if file.endswith('zip')]
        if input_args.maxquant_file in os.listdir(dirpath):
            zip_files = zip_files + _loose_to_zip(dirpath)
        if not zip_files:
            return

        accnr = _get_accnr(input_args, dirpath, zip_files, multiprocessing)

        if not multiprocessing and input_args.threads > 1:
            with Pool(min(len(zip_files), input_args.threads)) as ps:
                for i, _ in enumerate(ps.imap_unordered(partial(_local_iter, input_args=input_args, dirpath=dirpath, accnr=accnr, multiprocessing=True), zip_files)):
                    print(f'{i + 1} / {len(zip_files)}', end='\r')
        else:
            [_local_iter(f, input_args, dirpath, accnr, multiprocessing) for f in zip_files]

        if os.path.exists(f'{dirpath}working.zip'):
            os.remove(f'{dirpath}working.zip')

    except Exception as e:
        _ms2ai_log(f'{dirpath} (local), {e.__class__.__name__}: {e}', 'info')
