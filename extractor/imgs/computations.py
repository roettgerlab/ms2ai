import numpy as np

from extractor.common import _get_lower_bound
from extractor.common import _get_ms1_shape


def _lcms_edge_values(files_entry):
    mz_bounds = {'lower': files_entry['ms1 filter min'], 'upper': files_entry['ms1 filter max']}
    rt_bounds = {'lower': files_entry['ms1 min rt'], 'upper': files_entry['ms1 max rt']}

    return mz_bounds, rt_bounds


def _get_bins(input_args, files_entry):
    mz_bin = input_args.mz_bin
    rt_bin = input_args.rt_bin if not input_args.rt_as_percentage else \
             (files_entry['last peptide'] - files_entry['first peptide']) / 100 * input_args.rt_bin

    return {'mz': mz_bin, 'rt': rt_bin}


def _get_resolution(input_args, files_entry, as_string=False):
    bins = _get_bins(input_args, files_entry)
    mz_bounds, rt_bounds = _lcms_edge_values(files_entry)

    res = {'mz': int((mz_bounds['upper'] - mz_bounds['lower']) / bins['mz']),
           'rt': int((rt_bounds['upper'] - rt_bounds['lower']) / bins['rt'])}

    return res if not as_string else 'x'.join([str(f) for f in res.values()])


def _get_ranges(input_args, files_entry):
    bins = _get_bins(input_args, files_entry)
    res = _get_resolution(input_args, files_entry)
    mz_neighborhood, rt_neighborhood = _lcms_edge_values(files_entry)

    ranges = {'mz': np.linspace(mz_neighborhood['lower'], mz_neighborhood['upper'] + bins['mz'], res['mz']),
              'rt': np.linspace(rt_neighborhood['lower'], rt_neighborhood['upper'] + bins['rt'], res['rt'])}

    return ranges


def _get_window(center, input_args, ranges=None):
    shape = _get_ms1_shape(input_args.db_name)
    mz_target = _get_lower_bound(ranges['mz'], center['mz'])
    rt_taget = _get_lower_bound(ranges['rt'], center['rt'])
    mz_window = {'lower': mz_target - int(shape[1] / 2),
                 'upper': mz_target + int(shape[1] / 2) + 1}
    rt_window = {'lower': rt_taget - int(shape[0] / 2),
                 'upper': rt_taget + int(shape[0] / 2) + 1}

    return mz_window, rt_window


def _horizontal_flatten(arr):
    try:
        return np.hstack(arr)
    except:
        return arr


def _bin_query(value_range, target, i):
    return (value_range[i] <= target) & (target < value_range[i + 1])


def _combine_dimensions(ranges, image_ms1):
    rt_binned_ints = [_horizontal_flatten(image_ms1['int'][_bin_query(ranges['rt'], image_ms1['rts'], i)]) for i in range(len(ranges['rt'][:-1]))]
    rt_binned_mzs = [_horizontal_flatten(image_ms1['mz'][_bin_query(ranges['rt'], image_ms1['rts'], i)]) for i in range(len(ranges['rt'][:-1]))]
    binned_ints = np.asarray([[ints[_bin_query(ranges['mz'], mz, i)] for i in range(len(ranges['mz'][:-1]))] for ints, mz in zip(rt_binned_ints, rt_binned_mzs)], dtype=object)
    binned_mzs = np.asarray([[mz[_bin_query(ranges['mz'], mz, i)] for i in range(len(ranges['mz'][:-1]))] for mz in rt_binned_mzs], dtype=object)

    # Adds the mean, var and max values of intensity, along with amount of data points and highest m/z
    image = np.array([[[np.mean(int), np.var(int), max(int), len(int), np.float(mz[int == max(int)][0])] if int.any() else np.zeros(5)
                       for int, mz in zip(int_row, mz_row)]
                       for int_row, mz_row in zip(binned_ints, binned_mzs)], dtype=float)
    return image


def _get_scan(rawfile, scan_number: int, centroid: bool, remove_zeros: bool):
    from fisher_py.data.business import Scan
    scan = Scan.from_file(rawfile._raw_file_access, int(scan_number))
    scan.prefer_centroids = centroid
    mzs = np.array(scan.preferred_masses)
    ints = np.array(scan.preferred_intensities)
    filters = scan.scan_type
    precursor_mass = rawfile._get_scan_filter_precursor_mass_(scan_number)

    if remove_zeros:
        mzs = mzs[ints != 0]
        ints = ints[ints != 0]

    return {'mz': mzs, 'int': ints, 'filters': filters, 'precursor mass': precursor_mass, 'scan number': scan_number}
