import numpy as np


def _ms2_scan_name(mq_df):
    return [f for f in mq_df.columns if 'scan number' in f.lower()][0]


def _find_ms2_scan(pep):
    scan_name = [f for f in pep.keys() if 'scan number' in f.lower()][0]
    if ';' not in str(pep[scan_name]):
        return int(pep[scan_name])
    else:
        return pep[scan_name].split(';')[0]


def _ms2_lists(ms2_int, ms2_mz, noise_removal=0.05):
    noise_index = np.array(ms2_int) > (noise_removal * max(ms2_int))
    mz_array = np.array(ms2_mz)[noise_index]
    int_array = np.array(ms2_int)[noise_index]

    return mz_array, int_array, max(ms2_int)


def _ms2_numerical_bin(input_args, ms2_mz, ms2_int):
    ms_info = [[0]] * input_args.bin_amount
    ratio = input_args.fixed_mz_length / input_args.bin_amount
    for mz, ints in zip(ms2_mz, ms2_int):
        indx = np.int(min(mz / ratio, input_args.bin_amount - 1))
        if ms_info[indx] == [0]:
            ms_info[indx] = [ints]
        else:
            ms_info[indx].append(ints)
    max_int = max(ms2_int)
    return [np.mean(f) / max_int for f in ms_info]


def _ms2_bin(input_args, ms2_int, ms2_mz):
    mz_list, int_list, max_int = _ms2_lists(ms2_int, ms2_mz)
    end_range = input_args.fixed_mz_length if input_args.fixed_mz_length else max(mz_list)
    hist = np.histogram(mz_list, bins=input_args.bin_amount, range=(0, end_range))

    # With intensity. Means all intensities and normalizes intensities of the ms2 spectrum
    if input_args.keep_intensities:
        ms2_info = _ms2_numerical_bin(input_args, mz_list, int_list)
    else:  # Without intensity
        ms2_info = np.array(hist[0] > 0, dtype=np.int32)

    ms2_info = np.array([ms2_info, hist[0]])
    ms2_size = ms2_info.shape

    return ms2_info, ms2_size


def _translate_ms2(input_args, mzs, ints):
    # Draw neighbourhood from large image
    if input_args.ms2_binning:
        # Binning the ms2 into 1 of 4 different binning methods
        (ms2info, ms2size) = _ms2_bin(input_args, ints, mzs)

    else:
        # Draw ms2 information from mzml file
        norm_mz = mzs / input_args.fixed_mz_length
        norm_int = ints / max(ints)
        ms2info = np.array([norm_mz, norm_int])
        ms2size = ms2info.shape

    return ms2info, ms2size


def _get_ms2(input_args, scan):
    # Extract fragmentation info
    name_dict = {1: '', 2: '2nd ', 3: '3rd ', 4: '4th '}
    frag_infos = [f.split(' ')[0] for f in scan['filters'].split('@')[1:]]
    splits = [[i for i, g in enumerate(f) if g.isnumeric()][0] for f in frag_infos]
    frags = [frag[:split].upper() for split, frag in zip(splits, frag_infos)]
    NCEs = [float(frag[split:]) for split, frag in zip(splits, frag_infos)]
    frag_info_list = [{f'{name_dict[i+1]}fragmentation': frag, f'{name_dict[i+1]}nce': nce} for i, (frag, nce) in enumerate(zip(frags, NCEs))]
    frag_info_dict = {key: value for combo in frag_info_list for key, value in combo.items()}
    mass_analyzer = {'mass analyser': scan['filters'].split(' ')[0]}

    ms2, ms2_shape = _translate_ms2(input_args, scan['mz'], scan['int'])
    ms2_info = {**{'ms2 shape': ms2_shape, 'used scan': scan['scan number'], 'scan precursor mz': scan['precursor mass']}, **frag_info_dict, **mass_analyzer}

    return {'scan': ms2, 'info': ms2_info}
