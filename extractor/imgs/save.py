import matplotlib
import numpy as np
from matplotlib import pyplot as plt

from extractor.imgs.computations import _lcms_edge_values


def _full_png_image(files_entry, image, path, size=(24, 12)):
    matplotlib.use('Agg')
    mz, rt = _lcms_edge_values(files_entry)
    x = np.arange(round(mz['lower'], 0), round(mz['upper'] + 50, 0), step=200, dtype=int)
    y = list(reversed(np.arange(0, round(rt['upper'] + 5, 0), step=5, dtype=int)))
    x_ticks = np.linspace(0, image[:, :, 0].shape[1], len(x), dtype=int)
    y_ticks = np.linspace(0, image[:, :, 0].shape[0], len(y), dtype=int)

    plt.figure(figsize=size)
    ax1 = plt.subplot2grid((2, 3), (0, 0), colspan=2, rowspan=1)
    ax1.set_title('Mean')
    ax1.imshow(image[:, :, 0], aspect='auto')

    ax2 = plt.subplot2grid((2, 3), (1, 0), colspan=1, rowspan=1)
    ax2.set_title('Standard Deviation')
    ax2.imshow(image[:, :, 1], aspect='auto')

    ax3 = plt.subplot2grid((2, 3), (0, 2), colspan=1, rowspan=1)
    ax3.set_title('Max')
    ax3.imshow(image[:, :, 2], aspect='auto')

    ax4 = plt.subplot2grid((2, 3), (1, 1), colspan=1, rowspan=1)
    ax4.set_title('Collapsed Count')
    ax4.imshow(image[:, :, 3], aspect='auto')

    ax5 = plt.subplot2grid((2, 3), (1, 2), colspan=1, rowspan=1)
    ax5.set_title('Highest intensity m/z')
    ax5.imshow(image[:, :, 4], aspect='auto')
    plt.setp((ax1, ax2, ax3, ax4, ax5), xticks=x_ticks, xticklabels=x, yticks=y_ticks, yticklabels=y)
    plt.tight_layout()
    plt.savefig(f'{path}.png')
    plt.close()
    plt.cla(); plt.clf()


def _pep_png_image(image, path, size=(5, 5)):
    matplotlib.use('Agg')
    image = np.ma.masked_equal(image, 0)
    plt.figure(figsize=size)
    ax1 = plt.subplot2grid((2, 4), (0, 0), colspan=2, rowspan=2)
    ax1.set_title('Mean')
    ax1.imshow(image[:, :, 0], aspect='auto')

    ax2 = plt.subplot2grid((2, 4), (0, 2), colspan=1, rowspan=1)
    ax2.set_title('Standard Deviation')
    ax2.imshow(image[:, :, 1], aspect='auto')

    ax3 = plt.subplot2grid((2, 4), (0, 3), colspan=1, rowspan=1)
    ax3.set_title('Max')
    ax3.imshow(image[:, :, 2], aspect='auto')

    ax4 = plt.subplot2grid((2, 4), (1, 2), colspan=1, rowspan=1)
    ax4.set_title('Collapsed Count')
    ax4.imshow(image[:, :, 3], aspect='auto')

    ax5 = plt.subplot2grid((2, 4), (1, 3), colspan=1, rowspan=1)
    ax5.set_title('Highest intensity m/z')
    ax5.imshow(image[:, :, 4], aspect='auto')
    plt.tight_layout()
    plt.savefig(f'{path}.png')
    plt.close()
    plt.cla(); plt.clf()
