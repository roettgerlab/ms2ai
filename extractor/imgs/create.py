import os

import numpy as np
import pandas as pd

from extractor.common import _import_table
from extractor.filters import _skip_custom, _skip_outofbounds
from extractor.imgs.computations import _combine_dimensions, _get_window, _get_ranges, _get_resolution, _get_scan
from extractor.imgs.ms2 import _find_ms2_scan, _get_ms2
from extractor.imgs.save import _full_png_image, _pep_png_image
from storage.db_init import _get_dbs, _insert_or_update
from storage.serialization import _np_save, _np_load, _create_file


def _imports(input_args, accnr, filename):
    pep_db = _get_dbs(input_args.db_name, 'peptides')
    files_entry = _get_dbs('ms2ai', 'files').find_one({'identifier': f'{accnr}/{filename}'})
    _create_file(f'{input_args.path}images/{input_args.db_name}/{accnr}/{filename}')

    return pep_db, files_entry


def _spectra_annotation(mq_df, ms2_info, scans, filepath):
    from fundamentals.annotation.annotation import annotate_spectra
    from fundamentals.mod_string import maxquant_to_internal
    annotation_df = pd.DataFrame(mq_df[['modified sequence', 'charge']].values, columns=['MODIFIED_SEQUENCE', 'PRECURSOR_CHARGE'])
    annotation_df['MASS_ANALYZER'] = [f['info']['mass analyser'] for f in ms2_info]
    annotation_df['SCAN_NUMBER'] = [f['info']['used scan'] for f in ms2_info]
    annotation_df['MZ'] = [f['mz'] for f in scans]
    annotation_df['INTENSITIES'] = [f['int'] for f in scans]
    annotation_df['MODIFIED_SEQUENCE'] = maxquant_to_internal(annotation_df['MODIFIED_SEQUENCE'].values)

    annotation = annotate_spectra(annotation_df)[3]
    annotation_save_df = pd.DataFrame([{**g, **{'SCAN_NUMBER': ff}} for f, ff in zip(annotation, annotation_df['SCAN_NUMBER'].values) for g in f])
    annotation_save_df = annotation_save_df[annotation_save_df['intensity'] > 0.05]

    return annotation_save_df.to_parquet(f'{filepath}annotated.parquet')


def _create_ms2(input_args, raw_file, filepath, multiprocessing, accnr, filename):
    mq_df = _import_table(f'{filepath}{input_args.maxquant_file}')

    scans = [_get_scan(raw_file, _find_ms2_scan(pep), True, True) for pep in mq_df.to_dict(orient='records')]
    ms2_info = [_get_ms2(input_args, scan) for scan in scans]
    if input_args.annotate_spectra and not os.path.exists(f'{filepath}annotated.parquet'):
        try:
            _spectra_annotation(mq_df, ms2_info, scans, filepath)
            _insert_or_update(_get_dbs('ms2ai', 'files'), {'annotation': True}, {'identifier': f'{accnr}/{filename}'})
        except Exception as e:
            _insert_or_update(_get_dbs('ms2ai', 'files'), {'annotation': False}, {'identifier': f'{accnr}/{filename}'})
            # print(f'{"/".join(filepath.split("/")[-3:-1])}: Cannot annotate spectra due to modification errors: {e}') if not multiprocessing else ''
            pass
    elif os.path.exists(f'{filepath}annotated.parquet') and not _get_dbs('ms2ai', 'files').find_one({'identifier': f'{accnr}/{filename}', 'annotation': True}):
        _insert_or_update(_get_dbs('ms2ai', 'files'), {'annotation': True}, {'identifier': f'{accnr}/{filename}'})
    return ms2_info


def _get_ms1_scans(raw_file):
    lcms = np.asarray([raw_file.get_scan_from_scan_number(int(f)) for f in raw_file._ms1_scan_numbers], dtype=object)[:, :3]
    lcms[:, 1] = [np.log(f) for f in lcms[:, 1]]
    return lcms


def _raw_image(input_args, raw_file, lcms, path, files_entry):
    ms1 = {'scans': raw_file._ms1_scan_numbers, 'rts': raw_file._ms1_retention_times}
    image_ms1 = {**ms1, **{'mz': lcms[:, 0], 'int': lcms[:, 1]}}
    ranges = _get_ranges(input_args, files_entry)
    image = _combine_dimensions(ranges, image_ms1)

    res = _get_resolution(input_args, files_entry)
    _np_save(image, f'{path}/{res["mz"]}x{res["rt"]}.txt')


def _create_ms1(input_args, raw_file, accnr, filename, multiprocessing):
    pep_db, files_entry = _imports(input_args, accnr, filename)
    res = _get_resolution(input_args, files_entry)
    # Create MS1
    if not os.path.exists(f'{input_args.path}{accnr}/{filename}/{res["mz"]}x{res["rt"]}.txt') or \
            len(_np_load(f'{input_args.path}{accnr}/{filename}/{res["mz"]}x{res["rt"]}.txt')[0, 0, :]) != 5:
        print(f'{accnr}/{filename}: Computing MS1 LC-MS presentation', end='\r') if not multiprocessing else ''
        lcms = _get_ms1_scans(raw_file)
        _raw_image(input_args, raw_file, lcms, f'{input_args.path}{accnr}/{filename}/', files_entry)

    # Save MS1 as PNG
    if not os.path.exists(f'{input_args.path}{accnr}/{filename}/{res["mz"]}x{res["rt"]}.png') and input_args.save_lcms_png:
        image = _np_load(f'{input_args.path}{accnr}/{filename}/{res["mz"]}x{res["rt"]}.txt')
        _full_png_image(files_entry, image[::-1], f'{input_args.path}{accnr}/{filename}/{res["mz"]}x{res["rt"]}', size=(20, 15))


def _sub_image(input_args, peptide, image, ms2, path, ranges, saves, pep_db):
    try:
        save_id = f'{path}{peptide["pep_id"]}'
        if pep_db.find_one({'_id': f'{path}{peptide["pep_id"]}'}) or _skip_custom(peptide):
            return
        pep = {'mz': peptide['ms/ms m/z' if 'ms/ms m/z' in peptide.keys() else 'm/z'], 'rt': peptide['retention time']}
        mz_window, rt_window = _get_window(pep, input_args, ranges=ranges)
        if _skip_outofbounds(mz_window, rt_window, ranges):
            return

        sub_image = image[rt_window['lower']:rt_window['upper'], mz_window['lower']:mz_window['upper']]
        ms1_info = {'ms1 shape': sub_image.shape}
        ms2_data = ms2['scan']
        save_data = np.asarray([sub_image if saves['ms1'] else False, ms2_data if saves['ms2'] else False], dtype=object)
        _np_save(save_data, f'{input_args.path}images/{save_id}.txt')
        pep_db.insert_one({**{'_id': f'{save_id}'}, **ms1_info, **peptide})
        if input_args.save_peptide_png:
            _pep_png_image(sub_image[::-1], f'{input_args.path}images/{save_id}', size=(5, 5))
    except Exception as e:
        print(e)


def _peptides(input_args, accnr, filename, peptides, multiprocessing, ms2):
    print(f'{accnr}/{filename}: Creating peptides presentation', end='\r') if not multiprocessing else ''
    files_entry = _get_dbs('ms2ai', 'files').find_one({'identifier': f'{accnr}/{filename}'})
    _create_file(f'{input_args.path}images/{input_args.db_name}/{accnr}/{filename}')

    res = _get_resolution(input_args, files_entry)
    image = _np_load(f'{input_args.path}{accnr}/{filename}/{res["mz"]}x{res["rt"]}.txt')
    img_path = f'{input_args.db_name}/{accnr}/{filename}/'
    saves = {'ms1': 'ms1' in input_args.extraction_level, 'ms2': 'ms2' in input_args.extraction_level}
    ranges = _get_ranges(input_args, files_entry)
    pep_db = _get_dbs(input_args.db_name, 'peptides')

    for i, (pep, scan) in enumerate(zip(peptides, ms2)):
        if i % 50 == 0:
            print(f'{accnr}/{filename}: Creating peptides presentation ({i}/{len(peptides)})                   ', end='\r') if not multiprocessing else ''
        _sub_image(input_args, pep, image, scan, img_path, ranges, saves, pep_db)
