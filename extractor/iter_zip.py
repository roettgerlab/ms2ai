import os
import re
from functools import partial
from multiprocessing import Pool
from shutil import copyfile
from zipfile import ZipFile

from extractor.file_metadata import _file, _match_files
from extractor.iter_raw import _iterate_rawfiles
from pride_api.common import _file_downloader
from utils.log import _ms2ai_log


def _raw_multi(input_args, multiprocessing, accnr, zip_file, file_links):
    zipfile = (zip_file.filename if isinstance(zip_file, ZipFile) else zip_file).split("/")[-1]
    if not multiprocessing and input_args.threads > 1:
        with Pool(min(len(file_links), input_args.threads)) as ps:
            for i, _ in enumerate(ps.imap_unordered(partial(_iterate_rawfiles, input_args=input_args, zipfile=zipfile.split('/')[-1], accnr=accnr, multiprocessing=True), file_links)):
                print(f'{i + 1} / {len(file_links)}', end='\r')
    else:
        [_iterate_rawfiles(f, input_args, zipfile, accnr, multiprocessing) for f in file_links]


def _pride_iter(zipfile, input_args, pre_acquired, accnr, rawfiles, multiprocessing):
    try:
        if bool(input_args.zip_files) and zipfile.split("/")[-1].split('.')[0] not in input_args.zip_files:
            return
        if not pre_acquired:
            _file_downloader(f'{input_args.path}temp', accnr, zipfile, multiprocessing, f'{accnr}-{zipfile.split("/")[-1]}')
            zipfile = ZipFile(f'{input_args.path}temp/{accnr}-{zipfile.split("/")[-1]}', 'r')
            mq_rawfiles, mq_df = _file(input_args, zipfile, accnr, multiprocessing)
            matched_files, matched_links = _match_files(input_args, rawfiles, mq_rawfiles, mq_df, accnr)
            zipfile.close()
            os.remove(zipfile.filename) if os.path.exists(zipfile.filename) else ''
        else:
            matched_files = [f for f in os.listdir(f'{input_args.path}{accnr}') if os.path.isdir(f'{input_args.path}{accnr}/{f}')]
            matched_links = matched_files

        file_links = [[f, g] for f, g in zip(matched_files, matched_links)]
        _raw_multi(input_args, multiprocessing, accnr, zipfile, file_links)

    except (KeyboardInterrupt, OSError) as e:
        quit(e)
    except Exception as e:
        _ms2ai_log(f'{accnr}/{(zipfile.filename.split("-", 1)[-1] if isinstance(zipfile, ZipFile) else zipfile).split("/")[-1]} (Zip file), {e.__class__.__name__}: {e}', 'info')
        pass


def _local_iter(zipfile, input_args, dirpath, accnr, multiprocessing):
    try:
        if bool(input_args.zip_files) and zipfile not in input_args.zip_files:
            return
        with ZipFile(f'{dirpath}{zipfile}', 'r') as zip_file:
            rawfiles, raw_df = _file(input_args, zip_file, accnr, multiprocessing)
            matched_files, matched_links = _match_files(input_args, rawfiles, rawfiles, raw_df, accnr)

        rawfiles = [f for f in matched_files if os.path.exists(f'{dirpath}{f}.raw') or f in dirpath]
        for file in rawfiles:
            filepath = f'{input_args.path}{accnr}/{file}/'
            if os.path.exists(f'{dirpath}{file}.raw') and not os.path.exists(f'{filepath}file.raw'):
                copyfile(f'{dirpath}{file}.raw', f'{filepath}file.raw')
            _raw_multi(input_args, multiprocessing, accnr, zipfile, [[file, file]])

    except (KeyboardInterrupt, OSError) as e:
        quit(e)
    except Exception as e:
        _ms2ai_log(f'{accnr}/{dirpath[re.search(accnr, dirpath).end():].replace("/", "")}/{zipfile} (Zip error): {e}', 'info')
        pass
