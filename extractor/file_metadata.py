import os
import re
from collections import Counter
from zipfile import ZipFile

import numpy as np
import pandas as pd

from extractor.common import _import_table
from extractor.imgs.ms2 import _ms2_scan_name, _find_ms2_scan
from extractor.xml_parser import _xml_parser
from pride_api.keys import _pride_keys
from storage.compressions import _ext_from_zip
from storage.db_init import _insert_or_update, _get_dbs
from storage.serialization import _create_file
from utils.errors import BrokenMQFile, RawfileMismatch, NotMaxquantZip


def _match_files(input_args, rawfiles, mq_rawfiles, mq_df, accnr):
    matches = np.asarray([(f, f.split('/')[-1].split('.')[0]) for f in rawfiles if f.split('/')[-1].split('.')[0] in mq_rawfiles])
    try:
        matched_links, matched_files = matches[:, 0], matches[:, 1]
    except:
        raise RawfileMismatch
    [_create_file(f'{input_args.path}{accnr}/{filename}/') for filename in matched_files]
    [pd.DataFrame.to_csv(mq_df[mq_df['raw file'] == filename], f'{input_args.path}{accnr}/{filename}/{input_args.maxquant_file}', index=False) for filename in matched_files]
    return matched_files, matched_links


def _maxquant(mq_files):
    # Import and concatenates all the mq files into one dataframe, Drops all na's in the Sequence and ms/ms scan number columns
    mq_df = pd.concat([_import_table(file=f, memory_file=True) for f in mq_files])
    mq_df.replace(' ', np.nan, inplace=True)
    mq_df.columns = [f.lower() for f in mq_df.columns]
    mq_df.dropna(subset=['sequence', _ms2_scan_name(mq_df)], inplace=True)
    mq_df['raw file'] = mq_df['raw file'].astype(str)
    rawfiles = list(set(mq_df['raw file']))
    return mq_df, rawfiles


def _summary_keys():
    return ['raw file', 'fraction', 'enzyme', 'enzyme mode', 'enzyme first search', 'use enzyme first search',
            'variable modifications', 'multi modifications', 'variable modifications first search', 'use variable modifications first search',
            'requantify', 'multiplicity', 'max. missed cleavages', 'max. labeled AAs', 'labels0', 'labels1', 'labels2',
            'lc-ms run type', 'time-dependent recalibration', 'ms', 'ms/ms', 'ms/ms submitted', 'ms/ms identified',
            'peptide sequences identified', 'peaks', 'peaks sequenced', 'peaks sequenced [%]', 'peaks repeatedly sequenced',
            'peaks repeatedly sequenced [%]', 'isotope patterns', 'isotope patterns sequenced', 'isotope patterns sequenced [%]',
            'isotope patterns repeatedly sequenced', 'isotope patterns repeatedly sequenced [%]', 'multiplets', 'multiplets sequenced',
            'multiplets sequenced [%]', 'multiplets identified', 'multiplets identified [%]', 'recalibrated', 'mbrfdr',
            'label free norm param', 'av. absolute mass deviation [ppm]', 'mass standard deviation [ppm]',
            'av. absolute mass deviation [mda]', 'nass standard deviation [mda]']


def _summary(summary_files, rawfiles, accnr):
    if bool(summary_files):
        # Import and concatenates all the summary files into one dataframe, and replaces any misnamed 'Raw file' column with the proper name, and retrieve all unique raw file names.
        summary = pd.concat([_import_table(file=f, memory_file=True) for f in summary_files])
        summary.columns = [f.lower() for f in summary.columns]
        # get a dictionary for each rawfile entry in the summary df, replace entries with '.' and insert informations
        summary_dicts = [summary[summary['raw file'] == f].iloc[0].to_dict() for f in rawfiles if f in list(summary['raw file'])]
        summary_dicts = [{k.replace('.', '_'): v if not isinstance(f[k], np.int64) else int(v) for k, v in f.items() if k in _summary_keys()} for f in summary_dicts]
        summary_dicts = [{**{'accession': accnr, 'raw file': str(f['raw file']), 'identifier': f'{accnr}/{f["raw file"]}', 'summary_exists': True}, **f} for f in summary_dicts]
        # insert the dictionary into the 'files' db
        [_insert_or_update(_get_dbs('ms2ai', 'files'), sum_file, ['identifier']) for sum_file in summary_dicts]

    else:
        # If no summary file exists, report that no summary file exists
        summary_missing = [{'accession': accnr, 'raw file': f, 'identifier': f'{accnr}/{f}', 'summary_exists': False} for f in rawfiles]
        [_insert_or_update(_get_dbs('ms2ai', 'files'), sum_file, ['identifier']) for sum_file in summary_missing]


def _project(accnr, rawfiles):
    file_info, projects = _get_dbs('ms2ai', 'files', 'projects')
    if bool(projects.find_one({'accession': accnr})):
        project_info = {k: v for k, v in projects.find_one({'accession': accnr}).items() if k in _pride_keys()}
        [_insert_or_update(file_info, {**{'accession': accnr, 'raw file': f, 'identifier': f'{accnr}/{f}', 'project_exists': True}, **project_info}, ['identifier']) for f in rawfiles]
    else:
        [_insert_or_update(file_info, {'accession': accnr, 'raw file': f, 'identifier': f'{accnr}/{f}', 'project_exists': False}, ['identifier']) for f in rawfiles]


def _file(input_args, zip_file, accnr, multiprocessing):
    zip_name = (zip_file.filename if isinstance(zip_file, ZipFile) else zip_file).split("/")[-1]
    mq_file, summary_file, xml_files = _ext_from_zip([input_args.maxquant_file, 'summary.txt', '.xml'], multiprocessing, zip_file=zip_file)
    print(f'{accnr}/{zip_name}: Handling metadata', end='\r') if not multiprocessing else ''
    if not mq_file:
        raise NotMaxquantZip
    mq_df, rawfiles = _maxquant(mq_file)
    _summary(summary_file, rawfiles, accnr)
    _project(accnr, rawfiles)
    try:
        _xml_parser(xml_files, accnr, rawfiles)
    except:
        pass
    return rawfiles, mq_df


def _raw_metadata(pep_df, accnr, filename, raw_file):
    mz_column = 'ms/ms m/z' if 'ms/ms m/z' in pep_df.columns else 'm/z'
    file_info = {'accession': accnr, 'raw file': filename, 'identifier': f'{accnr}/{filename}'}

    metadata = {'first peptide': min(pep_df['retention time']), 'last peptide': max(pep_df['retention time']),
                'lightest peptide': min(pep_df[mz_column]), 'heaviest peptide': max(pep_df[mz_column]),
                'peptide gradient length': max(pep_df['retention time']) - min(pep_df['retention time'])}

    if raw_file:
        min_filters = [float(f.split('-')[0]) for f in re.findall('\d*.\d*\-\d*\.\d*', raw_file.get_scan_from_scan_number(1)[-1])]
        max_filters = [float(f.split('-')[-1]) for f in re.findall('\d*.\d*\-\d*\.\d*', raw_file.get_scan_from_scan_number(1)[-1])]
        metadata.update({'ms1 min rt': raw_file._ms1_retention_times[0], 'ms1 max rt': raw_file._ms1_retention_times[-1],
                         'ms2 min rt': raw_file._ms2_retention_times[0], 'ms2 max rt': raw_file._ms2_retention_times[-1],
                         'gradient length': raw_file._ms1_retention_times[-1] - raw_file._ms1_retention_times[0],
                         'ms1 scans': len(raw_file._ms1_scan_numbers), 'ms2 scans': len(raw_file._ms2_scan_numbers),
                         'ms2 max mz': raw_file._raw_file_access.run_header.high_mass,
                         'ms1 filter min': min_filters if len(min_filters) > 1 else min_filters[0],
                         'ms1 filter max': max_filters if len(max_filters) > 1 else max_filters[0]})

    metadata = {**file_info, **metadata}
    _insert_or_update(_get_dbs('ms2ai', 'files'), metadata, ['identifier'])


def _add_identifiers(pep_df, accnr, filename):
    if '_id' in pep_df.columns:
        del pep_df['_id']

    if not all(col in pep_df.columns for col in ['accession', 'raw file', 'identifier', 'pep_id'] ):
        pep_df['accession'] = accnr
        pep_df['raw file'] = filename
        pep_df['identifier'] = f'{accnr}/{filename}'

        pep_df['pep_id'] = [f'{_find_ms2_scan(f)}-{f["sequence"]}' for i, f in pep_df.iterrows()]
        id_count = [f'-{f + 1}' if f else '' for f in list(pep_df.groupby(["pep_id"]).cumcount())]
        pep_df['pep_id'] = [f'{f["pep_id"]}{ff}' for (i, f), ff in zip(pep_df.iterrows(), id_count)]
    return pep_df


def _add_stats(pep_df, db_entry, raw_file):
    mz_column = 'ms/ms m/z' if 'ms/ms m/z' in pep_df.columns else 'm/z'

    if 'mass difference' not in pep_df.columns:
        # Compute linear values
        pep_df['linear m/z'] = (pep_df[mz_column] - db_entry['lightest peptide']) / (db_entry['heaviest peptide'] - db_entry['lightest peptide'])
        pep_df['linear rt'] = (pep_df['retention time'] - db_entry['first peptide']) / (db_entry['last peptide'] - db_entry['first peptide'])
        # Compute mass difference and recheck unmod status
        pep_df['mass difference'] = (pep_df[mz_column] - pep_df['m/z']) * pep_df['charge']
        pep_df['unmodified'] = (pep_df['modifications'] == 'Unmodified') & (pep_df['mass difference'] < 1)

    if raw_file is not None and 'precursor m/z difference' not in pep_df.columns:
        # Compute normalized values
        pep_df['normalized m/z'] = pep_df[mz_column] / db_entry['ms1 filter max']
        pep_df['normalized obs. m/z'] = pep_df[mz_column] / db_entry['heaviest peptide']
        pep_df['normalized rt'] = pep_df['retention time'] / db_entry['ms1 max rt']
        pep_df['normalized obs. rt'] = pep_df['retention time'] / db_entry['last peptide']
        # Compute precursor mz stats
        scan_precursor_mz = [raw_file._get_scan_filter_precursor_mass_(f) for f in [_find_ms2_scan(f) for i, f in pep_df.iterrows()]]
        pep_df['precursor m/z difference'] = np.abs(pep_df[mz_column] - scan_precursor_mz)

    return pep_df


def _add_ms2(pep_df, ms2):
    if ms2:
        ms2 = pd.DataFrame([f['info'] for f in ms2])
        for f in ms2:
            pep_df[f] = ms2[f]

        scan_count = Counter(ms2['used scan'])
        pep_df['#peptides for scan'] = [scan_count[f] for f in ms2['used scan']]

    return pep_df


def _handle_metadata(input_args, filepath, accnr, filename, raw_file, ms2):
    pep_df = _import_table(f'{filepath}{input_args.maxquant_file}')
    if len(pep_df) < 2:
        os.remove(f'{filepath}{input_args.maxquant_file}')
        raise BrokenMQFile
    pep_df.columns = [f.lower() for f in pep_df.columns]
    _raw_metadata(pep_df, accnr, filename, raw_file)
    db_entry = _get_dbs('ms2ai', 'files').find_one({'identifier': f'{accnr}/{filename}'})
    pep_df = _add_identifiers(pep_df, accnr, filename)
    pep_df = _add_stats(pep_df, db_entry, raw_file)
    pep_df = _add_ms2(pep_df, ms2)
    pd.DataFrame.to_csv(pep_df, f'{filepath}{input_args.maxquant_file}', index=False)
    pep_df = pep_df[pep_df['intensity'] > 0] if 'intensity' in pep_df.columns else pep_df
    if input_args.remove_duplicates:
        pep_df = pep_df.groupby('sequence', group_keys=False).apply(lambda x: x.loc[x.score.idxmax()])
    return pep_df.to_dict(orient='records')
