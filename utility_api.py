import argparse

from utils.common import _inital_path_setup, _compile_args
from utils.log import _setup_log


def _setup_args():
    parser = argparse.ArgumentParser(description='MS2AI Peptide metadata filtering')
    # Summary
    parser.add_argument('-s', '--summary', help='In Depth analysis of available metadata- and raw file data', action='store_true')
    # Tutorial
    parser.add_argument('-t', '--tutorial', help='In built tutorial, gathering, extracting and running a test network', action='store_true')
    # Package tester
    parser.add_argument('-p', '--package_test', help='Test if all packages are loaded and correctly installed', action='store_true')
    # Corrector
    parser.add_argument('-co', '--corrector', type=str, help='Corrects files from older versions')
    # Move
    parser.add_argument('-m', '--move', help='Move data and entries from one db to another for machine learning testing purposes', type=str, nargs='+', metavar="")
    parser.add_argument('-c', '--copy', help='Copy data and entries from one db to another for machine learning testing purposes', type=str, nargs='+', metavar="")
    parser.add_argument('-q', '--query', type=str, nargs='+', help='A query added to the filter, to give the user more control over the filtered peptides', metavar="", default=[])
    parser.add_argument('--filtered', help='Moves the filtered version instead of the un-filtered version', action='store_true')
    parser.add_argument('--physical', help='Move or copy the physical files', action='store_false')
    parser.add_argument('--db_entries', help='Move or copy the db entries files', action='store_false')
    parser.add_argument('-kr', '--keep_references', help='Does not change the location of the file, or database location, but merely copies the database', action='store_true')
    # Additional metadata
    parser.add_argument('-ap', '--add_metadata_projects', type=str, help='Add metadata to projects db based on path and format', metavar="")
    parser.add_argument('-af', '--add_metadata_files', type=str, help='Add metadata to files db based on path and format', metavar="")
    # Prepare dlomix
    parser.add_argument('-dlo', '--prepare_dlomix', help='Prepares dlomix output files for non ms2ai interaction with dlomix models', action='store_true')
    parser.add_argument('-db', '--db_name', type=str, help='Add information to the MS1 part of the peptide data representation', metavar="", default='ms2ai')
    # Setup
    parser.add_argument('-l', '--logging_level', type=str, help='Python logging level for internal error prints (https://docs.python.org/3/library/logging.html#logging-levels)', metavar="", default='info')
    parser.add_argument('-f', '--force_default', help='Saves the of all ms2 spectra regardless of spectra length', action='store_true')

    return parser


if __name__ == '__main__':
    _inital_path_setup()
    input_args = _compile_args(_setup_args())
    _setup_log(input_args)
    # Python package test!
    if input_args.package_test:
        from utils.functions.package_test import __package_test
        __package_test()

    if input_args.tutorial:
        from utils.functions.tutorial import full_tutorial
        full_tutorial()

    if input_args.corrector:
        from utils.functions.corrector import _run_corrector
        _run_corrector(input_args.corrector)

    if input_args.summary:
        from utils.functions.summary import _run_summary
        _run_summary(input_args)

    if input_args.move is not None or input_args.copy is not None:
        from utils.functions.move import _move_data
        _move_data(input_args)

    if input_args.prepare_dlomix:
        from utils.functions.dlomix import _prepare_dlomix
        _prepare_dlomix(input_args)

    if input_args.add_metadata_files or input_args.add_metadata_projects:
        from utils.functions.add_metadata import _add_metadata
        path = input_args.add_metadata_files if input_args.add_metadata_files else input_args.add_metadata_projects
        db = 'files' if input_args.add_metadata_projects else 'projects'
        _add_metadata(input_args, path, db)
