import os
from functools import partial
from glob import glob
from multiprocessing.pool import Pool

from pride_api.get_urls import _get_project
from scraper.computations import _get_all, _simplify
from scraper.file_metadata import _metadata_files
from storage.db_init import _get_dbs, _insert_or_update
from utils.common import _validated_input


def _scraper_init(input_args):
    [os.remove(f) for f in glob(f'{input_args.path}temp/*.zip')]

    projects = _get_dbs('ms2ai', 'projects')
    if input_args.accession:
        return projects

    if projects.count_documents({'source': 'pride'}) != 0 and _validated_input('Overwrite current project metadata originating from PRIDE?', ['y', 'n'], equals_to='y', default_option='n', force_default=input_args.force_default):
        projects.delete_many({'source': 'pride'})
    elif projects.count_documents({'source': 'pride'}) != 0 and _validated_input('Overwrite metadata entries with errors?', ['y', 'n'], equals_to='y', default_option='n', force_default=input_args.force_default):
        projects.delete_many({'maxquant_files': 'error: unknown zipfile error', 'source': 'pride'})
        projects.delete_many({'maxquant_files': {'$regex': f'error.+[0-{max(0, input_args.max_zipfile_size - 1)}]\.\d+'}, 'source': 'pride'})

    return projects


def _run_scraper(input_args):
    pride = _scraper_init(input_args)

    metadata = [_get_project(f) for f in input_args.accession] if input_args.accession else _get_all()
    metadata = _simplify(metadata)
    existing = pride.distinct('accession')
    metadata = [f for f in metadata if f['accession'] not in existing]
    print(f'{len(metadata)} new entries!', end='\n')

    # Multi-processed
    if input_args.threads != 1 and len(metadata) > 1:
        with Pool(min(input_args.threads, len(metadata))) as ps:
            for i, pride_results in enumerate(ps.imap_unordered(partial(_metadata_files, input_args=input_args, multiprocessing=True), metadata)):
                print(f'Pulling project metadata: {i} / {len(metadata)}', end='\r')
                if pride_results is not None:
                    _insert_or_update(pride, pride_results, ['accession'])
    # Single-threaded
    else:
        for i, entry in enumerate(metadata):
            print(f'Pulling project metadata: {i} / {len(metadata)}', end='\r')
            pride_results = _metadata_files(entry, input_args, False)
            if pride_results is not None:
                _insert_or_update(pride, pride_results, ['accession'])
    print('File metadata pulled!', end='\n')

# For debugging:
# Loose XML files: PXD017125
# zip XML files: PXD026828, PXD011164
# Loose MQ files: PXD023821
# Loose MQ files and xml file: PXD023540
# .zip: PXD027822
# .zip > 100gb: PXD022950
# .zip > 5gb: PXD025315
# .tar.gz: PXD001426
# .7z: PXD024198
# .rar: PXD023650
# [f for f in metadata if f['accession'] == 'PXD018154']
