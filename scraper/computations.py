from itertools import chain
from multiprocessing.pool import Pool

from pride_api.common import _get_json
from pride_api.get_urls import _base_metadata_page


def _metadata_extender(entry):
    inter_dict = {}
    for key in entry:
        if isinstance(entry[key], list) and entry[key] and isinstance(entry[key][0], dict):
            for lower_keys in entry[key][0]:
                if '@' not in lower_keys and 'cv' not in lower_keys and '_' not in key:
                    if lower_keys == 'name':
                        inter_dict[key] = entry[key][0][lower_keys]
                    else:
                        inter_dict[f'{key} {lower_keys}'] = entry[key][0][lower_keys]
        elif isinstance(entry[key], dict):
            pass
        else:
            inter_dict[key] = entry[key]
    [inter_dict.update({f: None}) for f in inter_dict if inter_dict[f] == "" or not inter_dict[f]]
    return inter_dict


def _get_all():
    info = _get_json('https://www.ebi.ac.uk/pride/ws/archive/v2/projects?page=1&sortDirection=DESC&sortConditions=submissionDate')['page']

    metadata = []
    with Pool(16) as ps:
        for i, pages in enumerate(ps.imap_unordered(_base_metadata_page, list(range(0, info['totalPages'])))):
            if i * 100 % 250 == 0:
                print(f'Pulling base metadata: {i*100}/{info["totalElements"]}', end='\r')
            metadata.append(pages)
    print('Base metadata pulled', end='\n')

    return list(chain(*metadata))


def _simplify(metadata):
    metadata = [_metadata_extender(f) for f in metadata]
    [f.update({'maxquant': 'maxquant' in str(f).lower(), 'source': 'pride'}) for f in metadata]
    return metadata
