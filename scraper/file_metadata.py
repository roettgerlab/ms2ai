import os
from zipfile import ZipFile

from pride_api.common import _file_downloader
from pride_api.get_urls import _get_zip_files, _get_extensions, _loose_MQs, _get_files_api, _get_raw_files
from pride_api.keys import _maxquant_file_names
from utils.log import _ms2ai_log


def _maxquant_fileinfo(input_args, pride_entry, zipfiles, max_zip, multiprocessing):
    if pride_entry['maxquant'] and zipfiles:
        smallest_zip = sorted(zipfiles, key=lambda x: x[1], reverse=True)[0][0]
        size = sorted(zipfiles, key=lambda x: x[1], reverse=True)[0][1]

        if size * 1e-9 > max_zip:
            return {'maxquant_files': f'error: zipfile size of {round(size * 1e-9, 2)}gb exceeds limit of {max_zip}gb.'}

        try:
            _file_downloader(f'{input_args.path}temp', pride_entry['accession'], smallest_zip, multiprocessing, f'{pride_entry["accession"]}-{smallest_zip.split("/")[-1]}')
            with ZipFile(f'{input_args.path}temp/{pride_entry["accession"]}-{smallest_zip.split("/")[-1]}', 'r') as zip_file:
                files_in_zip = list(set([files.orig_filename.split('/')[-1] for files in zip_file.filelist
                                         if files.orig_filename.split('/')[-1] in _maxquant_file_names()
                                         or 'xml' in files.orig_filename.split('/')[-1]]))
            os.remove(f'{input_args.path}temp/{pride_entry["accession"]}.zip')
            return {'maxquant_files': files_in_zip}

        except Exception as error:
            _ms2ai_log(f'Error found: {error}', 'info')
            return {'maxquant_files': 'error: unknown zipfile error'}

    elif pride_entry['maxquant'] and not zipfiles:
        return {'maxquant_files': 'error: zipfile not found'}

    return {}


def _metadata_files(pride_entry, input_args, multiprocessing):
    try:
        # Get the extensions
        files_api = _get_files_api(pride_entry['accession'])
        pride_entry.update(_get_extensions(files_api))

        # Retrieve the zip and loose files
        zipfiles = _get_zip_files(input_args, files_api, get_size=True)
        rawfiles = _get_raw_files(files_api)
        loose_files = _loose_MQs(files_api, input_args, multiprocessing)

        pride_entry['raw files'] = len(rawfiles)
        pride_entry['zip files'] = len(zipfiles)
        if loose_files:
            # Get the maxquant files from loose files
            pride_entry.update({'maxquant_files': [f.split('/')[-1] for f in loose_files]})
        elif zipfiles:
            # Get the maxquant files from the smallest zipfile
            pride_entry.update(_maxquant_fileinfo(input_args, pride_entry, zipfiles, input_args.max_zipfile_size, multiprocessing))
        else:
            pride_entry.update({'maxquant_files': 'error: no maxquant files found'})

        return pride_entry

    except Exception as e:
        _ms2ai_log(f'{e.__class__.__name__}: {e} ({pride_entry})', 'debug')
        pride_entry.update({'maxquant_files': f'error: {e.__class__.__name__}: {e}'})
        return pride_entry
