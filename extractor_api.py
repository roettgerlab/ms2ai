import argparse

from utils.log import _setup_log


def _setup_args():
    parser = argparse.ArgumentParser(description='MS2AI Peptide metadata filtering')
    # Inputs
    parser.add_argument('-i', '--inputs', type=str, nargs='+', help='Input sources for data extractions', metavar='', default=[])
    parser.add_argument('-raw', '--raw_files', type=str, nargs='+', help='Raw file(s) to extract from. In none are provided every raw file will be extracted', metavar='', default=[])
    parser.add_argument('-sraw', '--skip_raw_files', type=str, nargs='+', help='Raw file(s) to not extract from', metavar='', default=[])
    parser.add_argument('-zip', '--zip_files', type=str, nargs='+', help='Zip file(s) from which Raw files will be correlated and extracted. In none are provided every zip file will be extracted', metavar='', default=[])
    parser.add_argument('-p', '--pride', help='Run all suitable data from PRIDE', action='store_true')
    parser.add_argument('-r', '--reset', help='Remove all current peptides and their metadata', action='store_true')
    parser.add_argument('-rc', '--re_create', help='Remove all current peptides and their metadata', action='store_true')
    parser.add_argument('-q', '--query', type=str, nargs='+', help='Specify queries for local or pride data extractor. Same manner as in the filter', metavar="", default=[])
    parser.add_argument('-s', '--skip_existing', help='Skips accession or raw files with existing peptides', action='store_true')
    parser.add_argument('-m', '--metadata', help='Only goes through the metadata part of the extractor + ms1/ms2', action='store_true')
    parser.add_argument('-mo', '--maxquant_only', help='Only goes through the metadata part of the extractor', action='store_true')
    parser.add_argument('-dl', '--download', help='Downloads all files without further computations', action='store_true')
    # Parameters
    parser.add_argument('-mq', '--maxquant_file', type=str, help='Name of the MaxQuant files to cross-reference peptides', metavar='', default='evidence.txt')
    parser.add_argument('-el', '--extraction_level', type=str, nargs='+', help='MS levels to extract into peptide representations', metavar='', default=['ms1', 'ms2'])
    parser.add_argument('-mzb', '--mz_bin', type=float, help='Bin size of m/z bins in m/z', metavar="", default=0.5)
    parser.add_argument('-rtb', '--rt_bin', type=float, help='Bin size of retention time bins in minutes (if not -rp)', metavar="", default=0.1)
    parser.add_argument('-mzi', '--mz_interval', type=float, help='Neighbourhood m/z interval for peptide images', metavar="", default=50)
    parser.add_argument('-rti', '--rt_interval', type=float, help='Neighbourhood retention time interval for peptide images', metavar="", default=5)
    parser.add_argument('-rp', '--rt_as_percentage', help='Create bins based on the percentages between first and last registered peptide', action='store_false')
    parser.add_argument('-lcms_png', '--save_lcms_png', help='Save LCMS data created images as PNG files', action='store_false')
    parser.add_argument('-pep_png', '--save_peptide_png', help='Save peptide data created images as PNG files', action='store_true')
    # MS2 Handling
    parser.add_argument('-a', '--annotate_spectra', help='Annotates all the MS2 Spectra for Spectra fragmentation predictions', action='store_true')
    parser.add_argument('-b', '--ms2_binning', help='Apply binning to ms2 representation', action='store_false')
    parser.add_argument('--keep_intensities', help='Dichotomize bins instead of retaining intensity information', action='store_false')
    parser.add_argument('--variable_mz', help='Saves the of all ms2 spectra regardless of spectra length', action='store_false')
    parser.add_argument('--fixed_mz_length', type=int, help='Added to the filter to create n classes based on a list passed by the user', metavar="", default=2500)
    parser.add_argument('--bin_amount', type=int, help='Added to the filter to create n classes based on a list passed by the user', metavar="", default=500)
    # Setup
    parser.add_argument('-t', '--threads', type=int, help='Runs the scraper at n threads', metavar='', default=1)
    parser.add_argument('-db', '--db_name', type=str, help='Add information to the MS1 part of the peptide data representation', metavar="", default='ms2ai')
    parser.add_argument('-l', '--logging_level', type=str, help='Python logging level for internal error prints (https://docs.python.org/3/library/logging.html#logging-levels)', metavar="", default='info')
    parser.add_argument('-z', '--max_zipfile_size', type=int, help='Max zip file size (in GB) set to reduce the memory stress', metavar='', default=4)
    parser.add_argument('-f', '--force_default', help='Always choose the default for all user inputs (if one exists)', action='store_true')
    parser.add_argument('-rd', '--remove_duplicates', help='Extracts only the highest scoring peptides of the same sequence', action='store_true')

    return parser


if __name__ == '__main__':
    from utils.common import _compile_args, _inital_path_setup
    _inital_path_setup()

    from extractor.run import _run_extractor
    input_args = _compile_args(_setup_args())
    _setup_log(input_args)
    _run_extractor(input_args)
