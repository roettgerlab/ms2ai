from time import time

from filter.computations import _query, _inclusive_rawfiles, _get_class_list
from filter.save import _save_as_class, _save_as_is
from storage.db_init import _get_dbs


def _set_init(input_args):
    print(f'Initializing Parameters{"": <50}', end='\r')
    peptides, filtered = _get_dbs(input_args.db_name, 'peptides', 'filtered')
    file_info = _get_dbs('ms2ai', 'files')
    class_col = peptides if peptides.find_one({input_args.category: {'$exists': True}, '_id': {'$ne': 'sizes'}}) else file_info
    if class_col.find_one({input_args.category: {'$exists': True}}) is None:
        quit(f'Error: No peptides exists with class "{input_args.category}"')

    return file_info, peptides, filtered, class_col


def _run_filter(input_args):
    start = time()
    file_info, peptides, filtered, class_col = _set_init(input_args)
    peps, files_lookup = _query(input_args, peptides, file_info, class_col)
    if not peps:
        quit('No peptides follow all the requirements')
    if input_args.binary_class or input_args.class_count or input_args.class_list:
        comp_class_list = _get_class_list(input_args, peps, files_lookup)
        if input_args.class_representation and class_col.name != 'files':
            peps = _inclusive_rawfiles(input_args, peps, comp_class_list)

        filtered.drop() if not input_args.no_reset else ''
        _save_as_class(input_args, comp_class_list, peps, filtered, files_lookup)
    else:
        filtered.drop() if not input_args.no_reset else ''
        _save_as_is(input_args, peps, filtered, files_lookup)

    print(f'Time elaborated: {round((time()-start) / 60, 1)}min | Per peptides: {round((time()-start) / peptides.count_documents({}), 2)} seconds', end='\n')
