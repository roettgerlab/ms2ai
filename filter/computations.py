import re
from collections import defaultdict, Counter

from extractor.common import _get_common_ms1shape


def _inclusive_rawfiles(input_args, peps, class_list):
    print('Removing peptides from raw files without all classes present', end='\r')
    class_dict = defaultdict(list)
    [class_dict[f['raw file']].append(f[input_args.category]) for f in peps]  # if not f[cla] in a[f['Raw file']]]

    bin_raws = [f for f in class_dict if set(class_dict[f]) - {input_args.binary_class}] if input_args.binary_class else  [f for f in class_dict if all(item in set(class_dict[f]) for item in class_list)]

    return [f for f in peps if f['raw file'] in bin_raws]


def _get_class_list(input_args, peps, files_lookup):
    peps = peps if not files_lookup else [{input_args.category: str(f)} for f in files_lookup.values()]
    if input_args.class_count is not None:
        return [f[0] for f in Counter([f[input_args.category] for f in peps]).most_common()][:input_args.class_count]
    elif input_args.class_list is not None:
        return input_args.class_list
    else:
        class_list = [f[0] for f in Counter([f[input_args.category] for f in peps]).most_common()]
        return [f for f in class_list if f == input_args.binary_class or input_args.binary_class not in f]


def _query_dict(input_args, peptides, files, query):
    operator_dict = {'=': '$eq', '!=': '$ne', '<': '$lt', '<=': '$lte', '>': '$gt', '>=': '$gte', 'in': '$in', 'nin': '$nin', 'contain': '$regex', 'ncontain': '$regex', 'exists': '$exists'}
    key, value = re.split('|'.join(list([f' {f} ' for f in operator_dict.keys()])), query)
    key_op = re.findall('|'.join(list([f' {f} ' for f in operator_dict.keys()])) + '|$', query)[0].strip()
    operator = operator_dict[key_op]
    collection = peptides if peptides.find_one({key: {'$exists': True}}) else files if files.find_one({key: {'$exists': True}}) else None
    if collection is None:
        print(f'{key} not found in any database, skipping!{"": <50}')
        return
    value = value.split(' ') if operator in ['$in', '$nin'] else int(value) if operator == '$exists' else value
    value_type = list if operator in ['$in', '$nin'] else bool if operator == '$exists' else type(collection.find_one({key: {'$exists': True}})[key])
    if key_op == 'ncontain':
        value = f'^((?!{value}).)*$'
    value_operator = {operator: re.escape(value_type(value)) if input_args.escape_regex else value_type(value) if operator == '$regex' else value_type(value)}

    return collection.name, {key: value_operator}


def _query(input_args, peptides, files, class_col):
    print(f'Creating peptides queries{"": <50}', end='\r')
    query_cols = {'peptides': {**{'_id': {'$ne': 'sizes'}}, **({'ms1 shape': _get_common_ms1shape(input_args.db_name)} if input_args.shape_query else {})},
                  'files': {'identifier': {'$exists': True}}}
    query_cols[class_col.name].update({input_args.category: {'$exists': True}})

    # Query
    [query_cols[_query_dict(input_args, peptides, files, f)[0]].update(_query_dict(input_args, peptides, files, f)[1]) for f in input_args.query] if input_args.or_query else {}
    # Or query
    query_cols[_query_dict(input_args, peptides, files, input_args.or_query[0])[0]].update(
        {'$or': [_query_dict(input_args, peptides, files, f)[1] for f in input_args.or_query]}) if input_args.or_query else ''
    # And query
    query_cols[_query_dict(input_args, peptides, files, input_args.and_query[0])[0]].update(
        {'$and': [_query_dict(input_args, peptides, files, f)[1] for f in input_args.and_query]}) if input_args.and_query else ''

    files_lookup = {f['identifier']: f[input_args.category] for f in files.find(query_cols['files'])} if class_col.name == 'files' else {}
    if query_cols['files'] != {'identifier': {'$exists': True}}:
        query_cols['peptides'].update({'identifier': {'$in': files.find(query_cols['files']).distinct('identifier')}})
    print(f'Finding queried peptides{"": <50}', end='\r')
    return [f for f in peptides.find(query_cols['peptides'])], files_lookup
