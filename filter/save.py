from collections import Counter, defaultdict
from itertools import chain
from random import shuffle

from pymongo.errors import BulkWriteError


def _peptide_filter(category, peptides, class_list, binary_class, files_lookup):
    if binary_class:
        peptides = [{**f, **{'label': 1 if (str(f[category]) if not files_lookup else str(files_lookup[f['identifier']])) == binary_class else 0}} for f in peptides]
        class_list = [f'Not {binary_class}', binary_class]

    else:
        peptides = [{**f, **{'label': class_list.index(str(f[category]) if not files_lookup else str(files_lookup[f['identifier']]))}} for f in peptides if (str(f[category]) if not files_lookup else str(files_lookup[f['identifier']])) in class_list]

    return peptides, class_list


def _balance_limiter(pep_list):
    data_dict = defaultdict(lambda: defaultdict(list))
    [data_dict[f['identifier']][f['label']].append(f['_id']) for f in pep_list]
    id_dict = {}

    for f in data_dict:
        [shuffle(data_dict[g]) for g in data_dict]
        least_common = min([len(data_dict[f][g]) for g in data_dict[f]])
        for g in data_dict[f]:
            data_dict[f][g] = data_dict[f][g][:least_common]
        id_dict[f] = list(chain(*data_dict[f].values()))

    pep_list = [f for f in pep_list if f['_id'] in id_dict[f['identifier']]]

    return pep_list


def _save_as_class(input_args, class_list, peptides, filtered, files_lookup):
    print('Writing peptides to filtered DB', end='\r')
    pep_list, class_list = _peptide_filter(input_args.category, peptides, class_list, input_args.binary_class, files_lookup)

    if input_args.balance_classes:
        pep_list = _balance_limiter(pep_list)
    if input_args.limit_output:
        pep_list = list(chain(*[[g for g in pep_list if g['label'] == f][:input_args.limit_output] for f in range(len(class_list))]))

    [print(f'{class_list[f[0]]}: {f[1]}', end='\n') for f in Counter([f['label'] for f in pep_list]).most_common()]

    filtered.insert_one({'_id': 'index', 'category':  input_args.category, 'classes': {str(i): f for i, f in enumerate(class_list)}, 'classification': True})
    filtered.insert_many(pep_list)
    print(f'{filtered.count_documents({})} peptides from {len(filtered.distinct("accession"))} projects', end='\n')


def _save_as_is(input_args, peptides, filtered, files_lookup):
    print('Writing peptides to filtered DB', end='\r')
    [f.update({'label': (f[input_args.category] if not files_lookup else files_lookup[f['identifier']]) if isinstance(input_args.overwrite_label, bool) else input_args.overwrite_label}) for f in peptides]
    if input_args.limit_output:
        shuffle(peptides)
        peptides = peptides[:input_args.limit_output]
    if input_args.linearize:
        min_label = min([f['label'] for f in peptides])
        max_label = max([f['label'] for f in peptides])
        [f.update({'label': (f['label'] - min_label) / (max_label - min_label)}) for f in peptides]
    if not filtered.find_one({'_id': 'index'}):
                filtered.insert_one({'_id': 'index', 'category':  input_args.category,
                                     'classes': {input_args.category: 1} if isinstance(input_args.overwrite_label, bool) else {'overwrite label 1': input_args.overwrite_label},
                                     'classification': False if isinstance(input_args.overwrite_label, bool) else True})
    elif not isinstance(input_args.overwrite_label, bool):
        classes = filtered.find_one({'_id': 'index'})['classes']
        filtered.update_one({'_id': 'index'}, {'$set': {'classes': {**classes, **{f'overwrite label {len(classes)+1}': input_args.overwrite_label}}}})

    try:
        filtered.insert_many(peptides, ordered=False, bypass_document_validation=True)
    except BulkWriteError as e:
        print(f"{len(e.details['writeErrors'])} duplicated database entries skipped{'': <50}")

    print(f'{filtered.count_documents({})} peptides from {len(filtered.distinct("accession"))} projects', end='\n')
